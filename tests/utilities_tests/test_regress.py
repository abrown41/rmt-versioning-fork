import pytest
import unittest
from rmt_utilities import regress as rg
from shutil import which

DUMMY_EXEC = which('true')
DUMMY_mpiRun = './tests/utilities_tests/bin/mockmpirun'


class regressTests(unittest.TestCase):
    @classmethod
    def setup_class(self):
        self.flist = [('this', 1), ('that', 2)]
        self.plist = [('those', 3)]
        self.rtests = rg.regress_report(failList=self.flist, passList=self.plist)

    def test_initclass(self):
        assert self.rtests.fails == self.flist
        assert self.rtests.passes == self.plist

    def test_regressError(self):
        with pytest.raises(rg.regressError) as E:
            raise rg.regressError(self.rtests)
        assert str(E.value).startswith("RMT calculation in")
        assert "this agrees to 1 decimal places" in str(E.value)

    def test_bool(self):
        assert not self.rtests

    def test_str(self):
        expected = """RMT calculation in: None
Failing comparisons:
==========
this agrees to 1 decimal places
that agrees to 2 decimal places

RMT calculation in: None
Passing comparisons:
==========
those agrees to 3 decimal places\n"""
        assert self.rtests.__str__() == expected


class execTests(unittest.TestCase):
    @classmethod
    def setup_class(self):
        from subprocess import call
        call(["mkdir", "tests/utilities_tests/regress_run"])
        call(["cp", "tests/utilities_tests/data/expec_v_all.neon00001000", "tests/utilities_tests/regress_run"])
        call(["cp", "tests/utilities_tests/data/expec_z_all.neon00001000", "tests/utilities_tests/regress_run"])
        call(["cp", "tests/utilities_tests/data/input.conf", "tests/utilities_tests/regress_run"])
        call(["cp", "-r", "tests/utilities_tests/regress_run", "tests/utilities_tests/inputs"])
        self.rtests = rg.RMT_regression_tests('tests/utilities_tests/bin/testlist.yml', DUMMY_EXEC, DUMMY_mpiRun)
        self.tc = rg.testcase("tests/utilities_tests/", DUMMY_EXEC, DUMMY_mpiRun)

    @classmethod
    def teardown_class(self):
        from subprocess import call
        call(["rm", "-rf", "tests/utilities_tests/regress_run"])
        call(["rm", "-rf", "tests/utilities_tests/inputs"])
        self.tc.cleanupdir()
        self.rtests.cleanup()

    def test_setup(self):
        assert [x.testdir == self.tc.testdir for x in self.rtests.tests]
        assert [x.mpirun == self.tc.mpirun for x in self.rtests.tests]
        assert [x.exec == self.tc.exec for x in self.rtests.tests]

    def test_run(self):
        assert self.tc.runTest()

    def test_runAllTests(self):
        assert self.rtests.runAllTests()

    def test_yamlerror(self):
        import io
        import contextlib
        f = io.StringIO()
        with contextlib.redirect_stdout(f):
            rg.RMT_regression_tests('./googlyboo', rmtexec=None)
        assert 'googlyboo cannot be found' in f.getvalue()

    def test_exec_error(self):
        localtc = rg.testcase("tests/utilities_tests/", which('false'), which('false'))
        with pytest.raises(rg.executionError) as E:
            localtc.runTest()
        assert str(E.value).startswith(f"RMT calculation in {localtc.rundir}")
        localtc.cleanupdir()

    def test_no_exec(self):
        with pytest.raises(PermissionError) as E:
            rg.testcase('tests/utilities_tests/', './tests/utilities_tests/data/H', which('false'))
        assert "Operation not permitted" in str(E.value)

    def test_mpirunError(self):
        with pytest.raises(rg.mpirunError) as E:
            raise rg.mpirunError
        assert str(E.value).startswith("RMT regression testing requires")

    @pytest.mark.skipif(which('mpirun') is not None, reason="must not have mpirun installed")
    def test_nompirun(self):
        with pytest.raises(rg.mpirunError) as E:
            rg.RMT_regression_tests('tests/utilities_tests/bin/testlist.yml', which('true'), None)
        assert str(E.value).startswith("RMT regression testing requires")

    def test_testcase_nodir(self):
        with pytest.raises(FileNotFoundError) as E:
            rg.testcase("this", "and", "that")
        assert "PosixPath('this')" in str(E.value)

    def test_testcase_noinputs(self):
        with pytest.raises(FileNotFoundError):
            rg.testcase("tests/utilities_tests/data", which('true'), which('true'))

    def test_runall_exec_error(self):
        from pathlib import Path
        lr = rg.RMT_regression_tests('tests/utilities_tests/bin/testlist.yml',
                                     which('false'), DUMMY_EXEC)
        for r in lr:
            r.mpirun = None
        results = lr.runAllTests()
        assert results[Path('tests/utilities_tests/')] == "Calculation did not execute correctly"
        lr.cleanup()

    def test_mvdir(self):
        from pathlib import Path
        from subprocess import call
        orig_path = self.tc.rundir
        call(["mkdir", orig_path])
        exp_path = Path(self.tc.testdir/"googlyboo")
        call(["mkdir", exp_path])
        self.tc.mvdir("googlyboo")
        assert (self.tc.rundir == exp_path)
        assert (not orig_path.is_dir())
        assert (exp_path.is_dir())
