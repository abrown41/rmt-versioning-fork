#!/bin/bash

mkdir -p outputs
rm -f *.dat fort.* dyson_*

# ----------------------------------------------------------------------------------------------------------------------------------
echo "H2O: cdenprop"
# ----------------------------------------------------------------------------------------------------------------------------------

ln -s ../1-integrals/moints               fort.17

ln -s ../2-inner/target-ci-coeffs.dat             fort.266
ln -s ../2-inner/target-properties.dat            fort.24

ln -s ../2-inner/scattering-ci-coeffs.dat         fort.25
ln -s ../2-inner/scattering-configurations-A1.dat fort.7001
ln -s ../2-inner/scattering-configurations-B1.dat fort.7002

cdenprop < inputs/cdenprop.inp \
               1> cdenprop.out \
               2> cdenprop.err || exit 1

mv fort.123 cdenprop-fort123.dat
mv fort.222 cdenprop-fort222.dat
mv fort.333 cdenprop-fort333.dat
mv fort.667 cdenprop-inner-dipoles.dat
mv fort.9978 cdenprop-fort9978.dat

mv dyson_orbitals.ukrmolp0 cdenprop-dyson_orbitals_ukrmolp0.dat
mv target.phases.data cdenprop-target.phases.dat

rm -f fort.17 fort.24 fort.25 fort.266 fort.7001 fort.7002

# ----------------------------------------------------------------------------------------------------------------------------------
echo "H2O: outer"
# ----------------------------------------------------------------------------------------------------------------------------------

ln -s ../1-integrals/moints        fort.22

ln -s ../2-inner/target-properties.dat     fort.24
ln -s ../2-inner/scattering-ci-coeffs.dat  fort.25

ln -s cdenprop-inner-dipoles.dat           fort.624

mpiexec -n 1 outer-run swintf rsolve_photo < inputs/outer.inp 1> outer.out 2> outer.err || exit 1

mv fort.10   outer-fort10.dat
mv fort.19   outer-fort19.dat
mv fort.21   outer-fort21.dat
mv fort.45   outer-fort45.dat
mv fort.123  outer-fort123.dat
mv fort.142  outer-partial-wave-dipoles.dat
mv fort.242  outer-fort242.dat
mv fort.699  outer-fort699.dat
mv fort.700  outer-fort700.dat

rm -f fort.22 fort.24 fort.25 fort.624
