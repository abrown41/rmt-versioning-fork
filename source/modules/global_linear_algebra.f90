! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles linear algebra (inner products) on arrays,
!> important tasks on the outer region numerical grid and collates distributed
!> population data.
MODULE global_linear_algebra

    USE precisn, ONLY: wp
    USE communications_parameters, ONLY: outer_group_id
    USE grid_parameters,           ONLY: x_1st, &
                                         x_last, &
                                         channel_id_1st, &
                                         channel_id_last, &
                                         my_channel_id_1st, &
                                         my_channel_id_last
    USE local_ham_matrix,          ONLY: rweight, &
                                         get_local_pop_outer
    USE mpi_communications,        ONLY: message_1st, &
                                         message_last, &
                                         group_first_pe_sums_message, &
                                         all_group_pes_sum_a_cmplx, &
                                         all_group_pes_sum_a_real
    USE rmt_assert,                ONLY: assert

        use mpi_communications, only: get_my_group_pe_id, i_am_in_first_outer_block, timeindex_val

    IMPLICIT NONE

CONTAINS

!-----------------------------------------------------------------------
! Inner prod = conjg(X)*Y =  <X, Y>, order imp.
!-----------------------------------------------------------------------

    REAL(wp) FUNCTION self_inner_product(del_R, Y)

        USE global_data, ONLY: one

        COMPLEX(wp) :: Y(x_1st:x_last, my_channel_id_1st:my_channel_id_last)
        REAL(wp)    :: del_R
        REAL(wp)    :: pe_sum, summed_ip
        INTEGER     :: psi_length

        psi_length = (my_channel_id_last - my_channel_id_1st + 1)*(x_last - x_1st + 1)

        pe_sum = self_local_inner_product(psi_length, del_R, Y)

        CALL all_group_pes_sum_a_real(pe_sum, summed_ip)

        self_inner_product = summed_ip

    END FUNCTION self_inner_product

!-----------------------------------------------------------------------
! Note: Sum = Sum + REAL(X(i))*REAL(Y(i)) + AIMAG(X(i))*AIMAG(Y(i)) !slower?
!-----------------------------------------------------------------------

    REAL(wp) FUNCTION self_local_inner_product(n, del_R, Y)

        COMPLEX(wp), INTENT(IN)  :: Y(*)
        REAL(wp), INTENT(IN)     :: del_R
        INTEGER, INTENT(IN)      :: n
        INTEGER     :: i
        REAL(wp)    :: sum

        sum = 0.0_wp
!$OMP   PARALLEL DO PRIVATE(I) REDUCTION(+:Sum)
        DO i = 1, n
        ! Hugo: rweight added
            sum = sum + CONJG(Y(i))*Y(i)*rweight(i)
        END DO
!$OMP   END PARALLEL DO

        self_local_inner_product = sum*del_R

    END FUNCTION self_local_inner_product

!-----------------------------------------------------------------------

    REAL(wp) FUNCTION real_inner_product(del_R, X, Y)
    use mpi_communications, only: get_my_pe_id
        USE global_data, ONLY: one

        COMPLEX(wp) :: X(x_1st:x_last, my_channel_id_1st:my_channel_id_last)
        COMPLEX(wp) :: Y(x_1st:x_last, my_channel_id_1st:my_channel_id_last)
        REAL(wp)    :: del_R
        REAL(wp) :: pe_sum, summed_ip
        INTEGER     :: psi_length
        integer  :: my_pe_id
        call get_my_pe_id(my_pe_id)
        psi_length = (my_channel_id_last - my_channel_id_1st + 1)*(x_last - x_1st + 1)

        pe_sum = real_local_inner_product(psi_length, del_R, X, Y)
        CALL all_group_pes_sum_a_real(pe_sum, summed_ip)

        real_inner_product = summed_ip

    END FUNCTION real_inner_product

!-----------------------------------------------------------------------

    REAL(wp) FUNCTION real_local_inner_product(n, del_R, X, Y)

        COMPLEX(wp), INTENT(IN)  :: X(*)
        COMPLEX(wp), INTENT(IN)  :: Y(*)
        REAL(wp), INTENT(IN)     :: del_R
        INTEGER, INTENT(IN)      :: n
        INTEGER     :: i
        REAL(wp)    :: sum

        sum = 0.0_wp
!$OMP   PARALLEL DO PRIVATE(I) REDUCTION(+:Sum)
        DO i = 1, n
        ! Hugo Rweight added
            sum = sum + CONJG(X(i))*Y(i)*rweight(i)
        END DO
!$OMP   END PARALLEL DO

        real_local_inner_product = sum*del_R

    END FUNCTION real_local_inner_product

!-----------------------------------------------------------------------

    COMPLEX(wp) FUNCTION inner_product(del_R, X, Y)

        COMPLEX(wp) :: X(x_1st:x_last, my_channel_id_1st:my_channel_id_last)
        COMPLEX(wp) :: Y(x_1st:x_last, my_channel_id_1st:my_channel_id_last)
        COMPLEX(wp) :: pe_sum, summed_ip
        REAL(wp)    :: del_R
        INTEGER     :: psi_length

        psi_length = (my_channel_id_last - my_channel_id_1st + 1)*(x_last - x_1st + 1)

        pe_sum = local_inner_product(psi_length, del_R, X, Y)

        CALL all_group_pes_sum_a_cmplx(pe_sum, summed_ip)

        inner_product = summed_ip

    END FUNCTION inner_product

!-----------------------------------------------------------------------

    COMPLEX(wp) FUNCTION local_inner_product(n, del_R, X, Y)

        COMPLEX(wp), INTENT(IN)  :: X(*)
        COMPLEX(wp), INTENT(IN)  :: Y(*)
        REAL(wp), INTENT(IN)     :: del_R
        INTEGER, INTENT(IN)      :: n
        INTEGER       :: i
        COMPLEX(wp)   :: sum

        sum = (0.0_wp, 0.0_wp)
!$OMP   PARALLEL DO PRIVATE(I) REDUCTION(+:Sum)
        DO i = 1, n
        ! Hugo rweight added
            sum = sum + CONJG(X(i))*Y(i)*rweight(i)
        END DO
!$OMP   END PARALLEL DO

        local_inner_product = sum*del_R

    END FUNCTION local_inner_product

!-----------------------------------------------------------------------

    SUBROUTINE decrement_V_with_cX(n, c, X, V)

        COMPLEX(wp), INTENT(IN)    :: X(*)
        COMPLEX(wp), INTENT(INOUT) :: V(*)
        REAL(wp), INTENT(IN)       :: c
        INTEGER, INTENT(IN)        :: n
        INTEGER  :: i

!$OMP   PARALLEL DO PRIVATE(i)
        DO i = 1, n
            V(i) = V(i) - c*X(i)
        END DO
!$OMP   END PARALLEL DO

    END SUBROUTINE decrement_V_with_cX

!-----------------------------------------------------------------------

    SUBROUTINE increment_psi_with_zX(n, z, X, psi)

        COMPLEX(wp), INTENT(IN)    :: X(*)
        COMPLEX(wp), INTENT(INOUT) :: psi(*)
        COMPLEX(wp), INTENT(IN)    :: z
        INTEGER, INTENT(IN)        :: n
        INTEGER  :: i

!$OMP   PARALLEL DO PRIVATE(i)
        DO i = 1, n
            psi(i) = psi(i) + z*X(i)
        END DO
!$OMP   END PARALLEL DO

    END SUBROUTINE increment_psi_with_zX

!-----------------------------------------------------------------------

    SUBROUTINE decrement_V_with_zX(n, z, X, V)

        COMPLEX(wp), INTENT(IN)    :: X(*)
        COMPLEX(wp), INTENT(INOUT) :: V(*)
        COMPLEX(wp), INTENT(IN)    :: z
        INTEGER, INTENT(IN)        :: n
        INTEGER  :: i

!$OMP   PARALLEL DO PRIVATE(i)
        DO i = 1, n
            V(i) = V(i) - z*X(i)
        END DO
!$OMP   END PARALLEL DO

    END SUBROUTINE decrement_V_with_zX

!-----------------------------------------------------------------------

    SUBROUTINE get_global_pop_outer_L(del_R, my_psi, population_outer_L, &
                                      data_1st, data_last, pop_index, my_discarded_population_L)

        USE initial_conditions, ONLY: numsols => no_of_field_confs, no_of_pes_per_sector
        USE mpi_communications, ONLY: mpi_comm_region, mpi_comm_inter_block
        USE MPI

        REAL(wp),    INTENT(IN)    :: del_R
        INTEGER,     INTENT(IN)    :: data_1st, data_last, pop_index
        COMPLEX(wp), INTENT(IN)    :: my_psi(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp),    INTENT(INOUT) :: population_outer_L(data_1st:data_last, channel_id_1st:channel_id_last, &
                                      1:numsols)
        REAL(wp),    INTENT(IN)    :: my_discarded_population_L(my_channel_id_1st:my_channel_id_last, 1:numsols)

        REAL(wp)                   :: pe_sum(channel_id_1st:channel_id_last, 1:numsols), &
                                      pe_total(channel_id_1st:channel_id_last, 1:numsols)

        REAL(wp)   :: my_pop_outer
        INTEGER    :: i, j, err, ierr


        ! start with local discarded pops (obtained from routine Split):
        pe_sum(:,:) = 0.0_wp
        pe_total(:,:) = 0.0_wp
        pe_sum(my_channel_id_1st:my_channel_id_last, :) = &
             my_discarded_population_L(my_channel_id_1st:my_channel_id_last, :)

!$OMP   PARALLEL DO PRIVATE(i, j, my_pop_outer) COLLAPSE(2)
        DO j = 1, numsols
            DO i = my_channel_id_1st, my_channel_id_last
                CALL get_local_pop_outer(del_R, my_psi(x_1st:x_last, i, j), my_pop_outer)
                pe_sum(i, j) = pe_sum(i, j) + my_pop_outer
            END DO
        END DO
!$OMP   END PARALLEL DO

        IF (no_of_pes_per_sector > 1) then
            CALL MPI_ALLREDUCE(pe_sum, pe_total, (channel_id_last - channel_id_1st + 1) * numsols, &
                        MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_region, ierr)
             population_outer_L(pop_index, channel_id_1st:channel_id_last, :) = &
                         pe_total(channel_id_1st:channel_id_last, :)
        ELSE
            !    Note that if no_of_pes_per_sector = 1, mpi_comm_inter_block == mpi_comm_region
             CALL MPI_REDUCE(pe_sum, pe_total, (channel_id_last - channel_id_1st + 1) * numsols, &
                        MPI_DOUBLE_PRECISION, MPI_SUM, 0, mpi_comm_region, ierr)

             population_outer_L(pop_index, :, :) = pe_total(:, :)
             !    Needs double-checking, however this population doesn't seem to be used anywhere.
!                The following lines can hopefully be deleted:
!        population_outer_L(pop_index, my_channel_id_1st:my_channel_id_last, :) = &
!                         pe_total(my_channel_id_1st:my_channel_id_last, :)
!             population_outer_L(pop_index, channel_id_1st:channel_id_last, :) = &
!                         pe_sum(channel_id_1st:channel_id_last, :)
        END IF
         
    END SUBROUTINE get_global_pop_outer_L

!-----------------------------------------------------------------------

    SUBROUTINE get_global_pop_outer(del_R, my_psi, population_outer, my_discarded_population, &
                                    population_outer_si, my_discarded_population_si, population_outer_ryd, single_ioniz_bndry)

        USE initial_conditions, ONLY: numsols => no_of_field_confs
        USE local_ham_matrix,   ONLY: get_local_pop_outer_si, &
                                      get_local_pop_outer_ryd
        USE lrpots,             ONLY: etuu
!    USE calculation_parameters,    ONLY: mpi_comms_method_desired, use_mpi_collective, use_sends_recvs

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: my_psi(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(OUT)   :: population_outer(1:numsols)
        REAL(wp), INTENT(IN)    :: my_discarded_population(my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(OUT)   :: population_outer_si(1:numsols)
        REAL(wp), INTENT(IN)    :: my_discarded_population_si(my_channel_id_1st:my_channel_id_last, 1:numsols)
        INTEGER, INTENT(IN)     :: single_ioniz_bndry
        REAL(wp), INTENT(OUT)   :: population_outer_ryd(1:numsols)

        REAL(wp) :: message(message_1st:message_last)
        REAL(wp) :: summed_array(message_1st:message_last)
        REAL(wp) :: my_pop_outer, my_pop_outer_SI, my_pop_outer_Ryd
        INTEGER  :: channel_id, isol
        REAL(wp) :: pe_sum(3)
        integer :: my_group_pe_id
        call get_my_group_pe_id(my_group_pe_id)
        
        DO isol = 1, numsols

            pe_sum = 0.0_wp

            DO channel_id = my_channel_id_1st, my_channel_id_last

                  
                CALL get_local_pop_outer(del_R, my_psi(:, channel_id, isol), my_pop_outer)

                CALL get_local_pop_outer_si(del_R, my_psi(:, channel_id, isol), my_pop_outer_si, single_ioniz_bndry)

                IF (etuu(channel_id) .GT. 1e-10) THEN
                    CALL get_local_pop_outer_ryd(del_R, my_psi(:, channel_id, isol), my_pop_outer_ryd, single_ioniz_bndry)
                ELSE
                    my_pop_outer_ryd = 0.0_wp
                END IF

                ! Add on local discarded pops (obtained from routine Split):

                my_pop_outer = my_pop_outer + my_discarded_population(channel_id, isol)
                my_pop_outer_si = my_pop_outer_si + my_discarded_population_si(channel_id, isol)

                pe_sum(1) = pe_sum(1) + my_pop_outer
                pe_sum(2) = pe_sum(2) + my_pop_outer_si
                pe_sum(3) = pe_sum(3) + my_pop_outer_ryd

                
            END DO  ! Loop over channels
            
            message = 0.0_wp
            message(1) = pe_sum(1)
            message(2) = pe_sum(2)
            message(3) = pe_sum(3)

            CALL assert((3) .LE. message_last, 'Message size too small')
            CALL group_first_pe_sums_message(message, summed_array, outer_group_id)
            population_outer(isol) = summed_array(1)
            population_outer_si(isol) = summed_array(2)
            population_outer_ryd(isol) = summed_array(3)
        END DO

    END SUBROUTINE get_global_pop_outer

!------------------------------------------------------------------

    SUBROUTINE get_global_expec_Z_outer(my_expec_z, expec_Z_all_outer)

        use mpi_communications, only: get_my_pe_id, get_my_group_pe_id
        COMPLEX(wp), INTENT(IN)  :: my_expec_z
        COMPLEX(wp), INTENT(OUT) :: expec_Z_all_outer
        REAL(wp) :: message(message_1st:message_last)
        REAL(wp) :: summed_array(message_1st:message_last)
!        integer :: my_pe_id, my_group_pe_id

!        call get_my_pe_id(my_pe_id)
!        call get_my_group_pe_id(my_group_pe_id)
        message = 0.0_wp
        message(1) = REAL(my_expec_z,wp)

        CALL group_first_pe_sums_message(message, summed_array, outer_group_id)

        expec_Z_all_outer = summed_array(1)

    END SUBROUTINE get_global_expec_Z_outer

!-----------------------------------------------------------------------

    SUBROUTINE get_population_at_a_pt(del_R, my_psi, my_pop_at_pt, my_tot_pop_at_pt)

      USE mpi_communications, only: mpi_comm_block
      USE MPI
        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: my_psi(x_1st, my_channel_id_1st:my_channel_id_last) ! ????
        REAL(wp), INTENT(OUT)   :: my_pop_at_pt, my_tot_pop_at_pt

        INTEGER :: channel_id, ierr

        my_pop_at_pt = 0.0_wp
        DO channel_id = my_channel_id_1st, my_channel_id_last  ! ???????????????
            my_pop_at_pt = my_pop_at_pt + &
                           REAL(my_psi(x_1st, channel_id)*CONJG(my_psi(x_1st, channel_id)), wp)
        END DO
        call MPI_ALLREDUCE(my_pop_at_pt, my_tot_pop_at_pt, 1, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_block, &
             ierr)
        my_pop_at_pt = my_pop_at_pt*del_R
        my_tot_pop_at_pt = my_tot_pop_at_pt*del_R

    END SUBROUTINE get_population_at_a_pt

!-----------------------------------------------------------------------

    SUBROUTINE get_global_norm_outer(del_R, psi, norm_outer)

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: psi(x_1st:x_last, my_channel_id_1st:my_channel_id_last)
        REAL(wp), INTENT(OUT)   :: norm_outer

        REAL(wp) :: message(message_1st:message_last)
        REAL(wp) :: summed_array(message_1st:message_last)
        REAL(wp) :: my_population_sum
        REAL(wp) :: sum_population, my_pop_outer
        INTEGER  :: channel_id
        integer :: my_group_pe_id
        call get_my_group_pe_id(my_group_pe_id)

        sum_population = 0.0_wp

        DO channel_id = my_channel_id_1st, my_channel_id_last
            CALL get_local_pop_outer(del_R, psi(:, channel_id), my_pop_outer)
            sum_population = sum_population + my_pop_outer
        END DO  ! Loop over all my channels
        my_population_sum = sum_population*del_R

        message = 0.0_wp
        message(1) = my_population_sum

        ! Sum population from all PE's in outer region
        CALL group_first_pe_sums_message(message, summed_array, outer_group_id)

        ! Need to keep norm_outer as square of norm for now as need to
        ! add norm_inner to it to get norm_all
        norm_outer = summed_array(1)
    END SUBROUTINE get_global_norm_outer

!-----------------------------------------------------------------------

    SUBROUTINE split(del_R, psi, discarded_population_outer, &
                     discarded_population_outer_si, single_ioniz_bndry)

        USE initial_conditions, ONLY: numsols => no_of_field_confs
        USE local_ham_matrix,   ONLY: local_split

        REAL(wp), INTENT(IN)       :: del_R
        COMPLEX(wp), INTENT(INOUT) :: psi(x_1st:x_last, my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(OUT)      :: discarded_population_outer(my_channel_id_1st:my_channel_id_last, 1:numsols)
        REAL(wp), INTENT(OUT)      :: discarded_population_outer_si(my_channel_id_1st:my_channel_id_last, 1:numsols)
        INTEGER, INTENT(IN)        :: single_ioniz_bndry
        INTEGER                    :: channel_id, isol

        DO isol = 1, numsols
            DO channel_id = my_channel_id_1st, my_channel_id_last
                CALL local_split(del_R, psi(:, channel_id, isol), discarded_population_outer(channel_id, isol), &
                                 discarded_population_outer_si(channel_id, isol), single_ioniz_bndry)
            END DO
        END DO

    END SUBROUTINE split

END MODULE global_linear_algebra
