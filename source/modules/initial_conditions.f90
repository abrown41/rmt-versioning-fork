! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets initial conditions for system: atomic/molecular/laser parameters,
!> parallel set up, checkpointing etc. Most parameters set in this module are
!> read from input.conf.

MODULE initial_conditions

    USE precisn,     ONLY: wp
    USE global_data, ONLY: pi

    IMPLICIT NONE

    INTEGER, PARAMETER :: RMatrixI_format_id = 1
    INTEGER, PARAMETER :: RMatrixII_format_id = 2

    Integer, PARAMETER :: LS_coupling_id = 1
    Integer, PARAMETER :: jK_coupling_id = 2

    INTEGER, PARAMETER :: length_gauge_id=1
    INTEGER, PARAMETER :: velocity_gauge_id=2

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !           TARGET INFORMATION          !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    

    !> Magnetic quantum number of the ground state.
    !> For molecular calculations this parameter is ignored.
    !> @ingroup target_info
    INTEGER  :: ml = 0
    !> Defines the sum, modulo 2, of the angular momentum and
    !> parity of the ground state. EG for the ground state of Ne
    !> (singlet S even) L=0 and pi=0 thus lplusp = 0.
    !! @ingroup target_info
    INTEGER  :: lplusp = 0
    !> Maximum absolute value of ML to be used in the calculation.
    !> @ingroup target_info
    INTEGER  :: ML_max = -1
    !> Set a restriction on ML_max?
    LOGICAL  :: set_ML_max = .false.
    !> Defines which of the included symmetries is the ground state.
    !! @ingroup target_info
    INTEGER  :: gs_finast = 1      
    !> Defines if the calculation is for an atomic / molecular target.
    !> By default, the code assumes atomic.
    !! @ingroup target_info
    LOGICAL  :: molecular_target = .false.
    !> Defines which version of the RMatrix inner region codes used to generate the input data.
    !> The default is RMatrixII (2) but for relativistic calculations RMatrixI (1) will be used.
    !! @ingroup target_info
    INTEGER  :: dipole_format_id = RMatrixII_format_id
    !> defines which coupling scheme to use. The default is LS coupling (1); jK coupling (2)
    !> can also be employed. For molecular calculations this parameter is ignored.
    !> @ingroup target_info
    INTEGER  :: coupling_id = LS_coupling_id
    !> Defines whether or not the ground state energy should be adjusted.
    !! @ingroup target_info
    LOGICAL  :: adjust_gs_energy = .false.
    !> Defines the desired ground state energy in a.u.
    !! @ingroup target_info
    REAL(wp) :: gs_energy_desired  = 0.0_wp

    ! Used to get number of highest eigenvalues removed from each symmetry block.
    ! A number of states need to be removed in order to maintain stability in the
    ! norm of the time-dependent wavefunctions.
    !
    ! States that need to be retained will have large contributions
    ! to the surface amplitude, typically of the order 0.1-1.0, whereas states
    ! with surface amplitudes of the order 1.0E-10 can be safely removed
    ! This is due to the Bloch operator, which leads to high energies for states
    ! with noticeable boundary amplitudes

    ! If Remove_Eigvals_Threshold is set to .true. then high lying eigenvalues
    ! with surface amplitudes > Surf_Amp_Threshold_Value are retained (recommended)

    ! If Remove_Eigvals_Threshold is set to .false. then the first symmetry block
    ! will have the last neigsrem_1st_sym eigenstates removed and the remaining
    ! symmetry blocks will have neigsrem_higher_syms eigenstates removed.

    !> (EXPERT) Removes highest energy eigenvalues from each state to ensure stability of norm.
    !> @ingroup target_info
    LOGICAL   :: remove_eigvals_threshold = .true.
    !> (EXPERT) Eigenvalues with surface amplitudes lower than `surf_amp_threshold_value` are discarded.
    !> @ingroup target_info
    REAL(wp)  :: surf_amp_threshold_value = 1.0_wp
    !> (EXPERT) Removes the last `neigsrem_1st_sym` eigenstates from the 1st symmetry block to ensure
    !> stability of the norm.
    !> @ingroup target_info
    INTEGER   :: neigsrem_1st_sym = 14
    !> (EXPERT) Removes `neigsrem_higher_syms` states from all remaining symmetry blocks.
    !> @ingroup target_info
    INTEGER   :: neigsrem_higher_syms = 20

    !> Set the maximum value of lambda to be included in the electron repulsion expansion
    !> @ingroup target_info
    INTEGER   :: reduced_lamax
    !> Set the maximum value of L (angular momentum) to be included. Note that for calculations
    !> employing jK coupling, L_max refers to the maximum value of 2J, so to include up to J=X 
    !> requires reduced_L_max = 2X.
    !> @ingroup target_info
    INTEGER   :: reduced_L_max

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !            LASER INFORMATION          !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    ! If use_2colour_field = .true.  then set parameters for IR pulse and for XUV pulse below, and delay parameters
    ! If use_2colour_field = .false. then just need to correctly set parameters for pulse labelled as IR pulse below
    !> Allows the use of two independently controllable pulses.
    !! @ingroup laser_params
    LOGICAL  :: use_2colour_field = .false.
    !> Add arbitrary field interpolated from file "EField.inp" (in the same format as the output EField.XXX)
    !! @ingroup laser_params
    LOGICAL  :: use_field_from_file = .false.
    !> Use an electric field derived from a carrier-envelope form vector potential (A-field) 
    !! @ingroup laser_params
    LOGICAL  :: carrier_envelope_afield = .false.
    ! IR Pulse (Or if using 1 colour pulse):
    !> Instead of a single XUV pulse, use a train of attosecond pulses (APT)
    !! @ingroup laser_params
    LOGICAL  :: use_pulse_train = .false.

    !> Number of different pulse characteristics to be solved in one run.
    !! @ingroup laser_params
    INTEGER :: no_of_field_confs = 1
    !> Input namelist limit on number of different pulse characteristics to be solved in one run.
    INTEGER, PARAMETER :: max_sols = 1000

    !> The ellipticity of the primary pulse.
    !> If Ellipticity is not set in the input.conf file, the primary pulse
    !> is linearly polarized in the z direction (i.e. ellipticity = 0).
    !> Ellipticity = +1 gives left-hand-circ. polarized in the zy plane with E x dE/dt pointing to +x
    !> Ellipticity = -1 gives right-hand-circ. polarized in the zy plane with E x dE/dt pointing to -x
    !! @ingroup laser_params
    REAL(wp)  :: ellipticity(max_sols) = 0.0_wp ! +1 for LH, -1 for RH

    !> Frequency of the (primary) IR laser pulse in a.u.
    !! @ingroup laser_params
    REAL(wp)  :: frequency(max_sols) = 1.0_wp ! 27.212.5 eV
    !> Sets both the number of cycles to be used in the ramp on and off part of the pulse.
    !! @ingroup laser_params
    REAL(wp)  :: periods_of_ramp_on(max_sols) = 2.5_wp
    !> Total number of field cycles including the ramp on and off.
    !> The pulse profile has a sin^2 envelope.
    !> Non-integer numbers of cycles are supported.
    !> periods_of_pulse will be greater than or equal to 2 * periods_of_ramp_on
    !! @ingroup laser_params
    REAL(wp)  :: periods_of_pulse(max_sols) = 5.0_wp
    !> The carrier envelope phase measured in degrees
    !! @ingroup laser_params
    REAL(wp)  :: ceo_phase_deg(max_sols) = 0.0_wp
    !> Peak intensity of the (primary) IR pulse in units of 10**14 Wcm^-2
    !! @ingroup laser_params
    REAL(wp)  :: intensity(max_sols) = 1.0_wp

    ! Orientation of the polarisation plane in degrees. The angles specify intrinsic x-z'-x'' rotation.
    ! Default orientation (when all angles are zero) is: Ex = 0, Ey ~ -e*sin(t), Ez ~ cos(t).
    ! Now, for example, if a linear polarisation with direction
    !     ( sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta) )
    ! is desired, set the angles to alpha = 0, beta = phi - 90, gamma = -theta, and Ellipticity to 0.
    !> @ingroup laser_params
    REAL(wp)  :: euler_alpha(max_sols) = 0.0_wp
    !> @ingroup laser_params
    REAL(wp)  :: euler_beta(max_sols) = 0.0_wp
    !> @ingroup laser_params
    REAL(wp)  :: euler_gamma(max_sols) = 0.0_wp

    ! Secondary (XUV) Pulse:

    !> If Ellipticity_XUV is not set in the input.conf file, the XUV pulse is
    !> is linearly polarized in the z direction
    !> Ellipticity_XUV = +1 gives circ. polarized in the zy plane with E x dE/dt pointing to +x
    !> Ellipticity_XUV = -1 gives circ. polarized in the zy plane with E x dE/dt pointing to -x
    !> Ellipticity_XUV = 0  gives linearly polarized in the z-direction
    !> unless Cross_Polarized = True in which case
    !> the XUV pulse is linearly polarized in the y-direction
    !> @ingroup laser_params
    REAL(wp)  :: ellipticity_XUV(max_sols) = 0.0_wp

    !> Cross_Polarized determines if the secondary field is to be arbitrarily
    !> polarized (which it is by default) or polarized at right angles to the
    !> primary field (cross_polarized = .true.)
    !> Note that setting (cross_polarized  = .true.) overrides the setting of
    !> Ellipticity, such that both the primary and secondary pulses are linearly
    !> polarized.
    !! @ingroup laser_params
    LOGICAL   :: cross_polarized(max_sols) = .false.

    !> Frequency of the XUV pulse in a.u.
    !! @ingroup laser_params
    REAL(wp)  :: frequency_XUV(max_sols) = 1.0_wp
    !> Sets both the ramp on and off
    !! @ingroup laser_params
    REAL(wp)  :: periods_of_ramp_on_XUV(max_sols) = 2.5_wp
    !> Total number of field cycles including the ramp on and off
    !! @ingroup laser_params
    REAL(wp)  :: periods_of_pulse_XUV(max_sols) = 5.0_wp
    !> The carrier envelope phase measured in degrees
    !! @ingroup laser_params
    REAL(wp)  :: ceo_phase_deg_xuv(max_sols) = 0.0_wp
    !> Peak intensity of the XUV pulse in units of 10**14 Wcm^-2
    !! @ingroup laser_params
    REAL(wp)  :: intensity_XUV(max_sols) = 1.0_wp

    !> Total number of XUV pulses in Attosecond pulse train
    !> Must have APT_bursts <= periods_of_pulse
    !! @ingroup laser_params
    REAL(wp) :: APT_bursts(max_sols) = 0.0_wp
    !> Number of XUV pulses in APT ramp on/off. 
    !! @ingroup laser_params
    REAL(wp) :: APT_ramp_bursts(max_sols) = 0.0_wp
    !> severity of switch on for each pulse in the APT. Higher is more sudden (and thus broader bandwidth)
    !! @ingroup laser_params
    REAL(wp) :: APT_envelope_power(max_sols) = 8.0_wp

    !> The default plane can be changed to the xy plane using this parameter.
    !> This may be desirable, particularly for circularly polarized laser pulses,
    !> as it restricts the included symmetries to the dipole accessible states,
    !> which is reduced in the xy plane compared to the yz. The number of outer
    !> region channels is also approximately halved due to the reduction in the
    !> number of magnetic sublevels.
    !! @ingroup laser_params
    LOGICAL   :: xy_plane_desired(max_sols) = .false.

    !> The time between the peak of the primary and secondary pulses
    !> measured in femtoseconds.
    !> Negative delay corresponds to the secondary pulse peak arriving first.
    !> Delay between fields is only used if use_2colour_field = .true. above
    !! @ingroup laser_params
    REAL(wp)  :: time_between_peaks_in_fs(max_sols) = 0.0_wp

    !> Alternative method of setting delay between pulses. Specify the delay between the start 
    !> of the two pulses in terms of the IR field cycles 
    !! @ingroup laser_params
    REAL(wp)  :: delay_XUV_in_IR_periods(max_sols) = -999

    !> The relative polarisation angle between the two fields
    !> measured in degrees.
    !> Angle between fields is only used if use_2colour_field = .true. above
    !! @ingroup laser_params
    REAL(wp)  :: angle_between_pulses_in_deg(max_sols) = 0.0_wp

    !> Power to which sin^2 envelope should be raised for primary pulse
    !! @ingroup laser_params
    INTEGER   :: envelope_power_primary(max_sols) = 1
    !> Power to which sin^2 envelope should be raised for secondary (XUV) pulse
    !! @ingroup laser_params
    INTEGER   :: envelope_power_secondary(max_sols) = 1

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !           I/O INFORMATION             !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !> Output the expectation value of the dipole (length and velocity) operators
    !! on each time-step. Used for calculating high-harmonic and absorption spectra.
    !! @ingroup io_params
    LOGICAL   :: dipole_output_desired = .false.
    !> Output the expectation value of the dipole velocity operator at each time-step.
    !! @ingroup io_params
    LOGICAL   :: dipole_velocity_output = .false.
    !>Choose X, Y or Z dipole expectation values. Default is Z.
    !! @ingroup io_params
    LOGICAL   :: dipole_dimensions_desired(1:3)  = (/ .FALSE.,.FALSE.,.TRUE./)
    !>Sets initial wavefunction to that in the data directory, irrespective of
    !> whether a restart from a checkpoint is required. Default is false.
    !> @ingroup io_params
    LOGICAL   :: read_initial_wavefunction_from_file=.false.
    !> A complete dump of all output data will be performed at every checkpoint,
    !> from which the calculation may be restarted.
    !> @ingroup io_params
    INTEGER   :: checkpts_per_run = 1
    !> The interval at which output data is written to file.
    !> Note that the electric field and dipole data are written at every timestep
    !> if dipole_output_desired is true.
    !! @ingroup io_params
    INTEGER   :: timesteps_per_output = 20
    !> Determines whether the detailed internal timing information is output by
    !> the inner/outer region masters and the first non-master outer region core
    !! @ingroup io_params
    LOGICAL   :: timings_desired = .false.
    !> Retains all previously output checkpoint data if set to true.
    !> This can be used to track the evolution of the wavefunction (for instance).
    !> @ingroup io_params
    LOGICAL   :: keep_checkpoints = .false.
    !> Write extra debugging information to the screen at run-time.
    !> @ingroup io_params
    LOGICAL   :: debug = .false.
    !> Output the population data as a single unformatted file.
    !> Saves substantil execution time for large calculations.
    !> @ingroup io_params
    LOGICAL   :: binary_data_files = .false.
    !> Write the ground state wavefunction to file.
    !> @ingroup io_params
    LOGICAL   :: write_ground = .true.
    !> Store the outer region population per channel
    !> @ingroup io_params
    LOGICAL   :: keep_popL = .true.
    !> Version identifier to suffix to all output files
    !> The final suffix also contains an intensity identifier
    !! @ingroup io_params
    CHARACTER(LEN=18)  :: version_root = ''

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !       PHYSICS INFORMATION             !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    

    !> Include long range interactions between the laser field and residual ion.
    !> @ingroup physics_params
    LOGICAL   :: wd_ham_interactions = .true.
    !> Include long range interactions between the ionised electron and residual ion.
    !> @ingroup physics_params
    LOGICAL   :: wp_ham_interactions = .true.
    !> Include long range interactions between the laser field and ejected electron.
    !> @ingroup physics_params
    LOGICAL   :: we_ham_interactions = .true.
    !> Tolerance for check of hermitian dipole. If the imaginary
    !> component of the dipole expectation value is larger than
    !> this value, RMT will raise an error
    !> @ingroup physics_params
    REAL(wp)  :: Hermitian_Tolerance = 10e-12_wp

    ! Parameters for windowing the dipole length expectation values

    !> If `window_harmonics == .true.` then a Gaussian mask is applied to the
    !> WP matrix so that the expectation value of the dipole is not dominated by
    !> contributions at large R (that wouldn't really contribute to harmonic
    !> generation. The position and shape of the mask are controlled by
    !> `window_cutoff` and `window_FWHM`.
    !> @ingroup physics_params
    LOGICAL :: window_harmonics = .true.
    !> The gaussian mask applied to the WP matrix starts at a radial coordinate
    !> `window_cutoff`.
    !> @ingroup physics_params
    REAL(wp) :: window_cutoff = 100.00_wp
    !> The gaussian mask applied to the WP matrix decays with a FWHM of
    !> `window_FWHM`.
    !> @ingroup physics_params
    REAL(wp) :: window_FWHM = 50.00_wp

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !    DOUBLE IONISATION INFORMATION      ! 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    

    !> Enable double ionization
    !> @ingroup double_params
    LOGICAL   :: double_ionization = .false.
    !> Include the two-electron Coulom repulsion interaction in the double outer region.
    !> @ingroup double_params
    LOGICAL   :: use_two_electron_we_ham = .true.
    !> Include interaction between two outer electrons and the laser field
    !> @ingroup double_params
    LOGICAL   :: use_two_electron_wp_ham = .true.
    !> The number of cores to be used for the two-electron outer region
    !! @ingroup double_params
    INTEGER   :: no_of_pes_to_use_double_outer = 0
    !> The two-electron outer region can be square or triangular.
    !! @ingroup double_params
    LOGICAL   :: square_double_outer_grid = .false.


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !    ABSORBING BOUNDARY INFORMATION     !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    

    !> Absorb_Desired determines whether the outgoing wavefunction will be
    !> suppressed at some radius in order to negate reflections from an
    !> insufficiently large outer region boundary. Default is .false.
    !> @ingroup absorb
    LOGICAL   :: absorb_desired = .false.
    !> Sigma factor determines the severity of the absorption. It is the Gaussian
    !> RMS width. A small sigma gives a more gradual absorption. Default is 0.2
    !> @ingroup absorb
    REAL(wp)  :: sigma_factor = 0.2_wp
    !> The fraction of the outer region box at which the Gaussian mask is applied.
    !> The default value of 0.7 means the mask starts 70% of the way into the
    !> outer region
    !> @ingroup absorb
    REAL(wp)  :: start_factor = 0.7_wp
    !> Number of time steps between Absorptions at the boundary.
    !> This strongly influences the strength of Absorption.
    !> If Delta_t is made smaller, get more absorption. Default is 20
    !> @ingroup absorb
    INTEGER   :: absorption_interval = 20

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !    PARALLELISATION INFORMATION        !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    

    !> (REQUIRED) Defines the number of cores to be used for the inner region calculation.
    !! @ingroup parallel_params
    INTEGER   :: no_of_pes_to_use_inner = -1
    !> (REQUIRED) The number of MPI tasks to be used for the (first) outer region calculation.
    !! @ingroup parallel_params
    INTEGER   :: no_of_pes_to_use_outer = -1
    !> (REQUIRED) The number of grid points allocated to the outer-region master
    !!(the first outer region core.) Due to the finite difference method employed,
    !! this value must be a multiple of four greater than or equal to twice
    !! the propagation_order. To achieve good load balancing this value should
    !! usually be chosen to be less than x_last_others as the outer-region
    !! master has additional work to perform.
    !! @ingroup parallel_params
    INTEGER   :: x_last_master = -1
    !> (REQUIRED) The number of grid points per outer region core. Due to the finite difference
    !! method employed, this value must be a multiple for four greater than or equal to
    !! twice the value of propagation_order
    !! @ingroup parallel_params
    INTEGER   :: x_last_others = -1
    !> The number of MPI tasks to be used per block for the outer region calculation.
    !! This defaults to 1. For the moment, it must be a factor of no_of_pes_to_use_outer.
    !! We also recommend that it should only be set without performing comparison tests if
    !! a larger number of cores are available than can be effectively used as OpenMP threads. 
    !! @ingroup parallel_params
    INTEGER   :: no_of_pes_per_sector = 1
    !> The number of OpenMP threads to employ in the inner region
    !! @ingroup parallel_params
    INTEGER   :: no_of_omp_threads_inner = 1
    !> The number of OpenMP threads to employ in the outer region
    !! @ingroup parallel_params
    INTEGER   :: no_of_omp_threads_outer = 1

    !> (EXPERT) The limit on the number of isends and irecvs used by a pe in live_communications.f90 routine
    !! parallel_matrix_vector_multiply (an array dimension). It may need to be increased if
    !! many_LML blcoks are used per pe. If it is too small the program halts with an error
    !! message.
    !! @ingroup parallel_params
    INTEGER   :: req_array_dim = 250
    !> (EXPERT) The limit on the number of (dipole) couplings to other blocks a particular LML block
    !! can have (array dimension). The array i_couple_to(:,:) is based in readhd.f90, allocated in
    !! distribute_hd_blocks.f90 and used there and in live_communications.f90. If it is too small
    !! the program termintates with an error message.
    !! @ingroup parallel_params
    INTEGER   :: i_couple_to_dim = 10
    !> (EXPERT) The limit on the number of LML blocks a pe can have. This is used for the array my_post in
    !! the programs tdse.f90 and GEtProcInfo.f90 and passed down to intialise.f90.
    !! If it is too small the program termintates with an error message.
    !! @ingroup parallel_params
    INTEGER   :: my_post_dim_lim = 100


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !    DISCRETISATION INFORMATION         !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    

    !> Choose the order of the Arnoldi/Taylor propagator 
    !! @ingroup discrete_params
    INTEGER   :: propagation_order = 8
    !> Determine if the Taylor series propagator is to be used
    !! @ingroup discrete_params
    LOGICAL   :: use_taylor_propagator = .false.

    ! Time step parameters:
    ! Steps_per_run is defined in calculation_parameters to
    ! ensure that it is a multiple of the various Checkpoint/Output parameters.
    ! The user should be able to specify any two of steps_per_run_approx, final_T and delta_t,
    ! The value of the third can be derived from the first two.
    ! delta_t can be defined here is defined as well in calculation parameters

    !> Defines the number of time-steps to be executed in the propagation.
    !> The actual number of time-steps will depend on the checkpointing conditions
    !! @ingroup discrete_params
    INTEGER   :: steps_per_run_approx = -1
    !> (REQUIRED) Defines the time, in atomic units, up to which the solution should be
    !> propagated. If `delta_t` is set then `final_T`/`delta_T` gives the number of time steps. 
    !> Or, if steps_per_run_approx is set, then, the time-step for the propagation is defined by
    !> `delta_t` = `final_t` / `steps_per_run_approx`, and should be ensured that this is
    !> sufficiently small to achieve convergence of the result. In practice we have
    !> found that a time-step of 0.01 a.u. gives good results for most applications.
    !! @ingroup discrete_params
    REAL(wp)  :: final_t = -1.00_wp ! Time in atomic units

    !> The time step in a.u to be used in the propagation. It should be ensured that this is
    !> sufficiently small to achieve convergence of the result. In practice we have
    !> found that a time-step of 0.01 a.u. gives good results for most applications.
    !! @ingroup discrete_params
    REAL(wp)  :: delta_t = 0.01_wp ! Time step in atomic units
    !> 
    !> Defines the outer-region grid spacing in atomic units.
    !> Numerical stability requires a small DeltaR.
    !> Note: size of outer region determined by the product:
    !> box_size = (no_of_pes_to_use_outer -1)* deltaR * x_last_others +
    !> deltaR * x_last_master.
    !! @ingroup discrete_params
    REAL(wp)  :: deltaR = 0.08_wp

    !> (EXPERT) Offset the outer region grid by a number of stepsizes (units of deltaR).
    !> This means that the first point in the outer region will be at
    !> b + offset_in_stepsizes*deltaR. Must be between 0.0 and 1.0. 
    !> Note that for larger offsets a smaller delta_t might be required.
    !! @ingroup discrete_params
    REAL(wp)  :: offset_in_stepsizes = 0.0_wp

    !> Determine which method use for calculating wavefunction time derivatives.
    !> Two methods are available, one using two fictitious grid points just inside
    !> the inner region boundary (H_inner), and another using a larger number of
    !> points (H_outer). H_outer is the default method, but the two point method
    !> can be enabled by setting the logical parameter below to true in the input
    !> file.

    !> If use_two_inner_grid_pts is .true., the value of delta_t may need to be 
    !> smaller than that typically used for the H_outer method. Tests have found
    !> that in some cases, the wavefunction norm was not conserved when a timestep
    !> delta_t = 0.01 was used with the two inner grid point method. In such cases, 
    !> delta_t had to be reduced to 0.0025 to ensure accurate norm conservation.
    !> The lack of norm conservation triggered the error message 'dipole expectation
    !> value is not real: is dipole operator hermitian?' in the output_dipole_files
    !> routine in the wavefunction module.  
    !! @ingroup discrete_params
    LOGICAL   :: use_two_inner_grid_pts = .false.


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    !           DERIVED PARAMETERS          !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    INTEGER   :: no_of_pes_to_use
    REAL(wp)  :: field_period_XUV(max_sols)
    REAL(wp)  :: ceo_phase_rad_xuv(max_sols)
    REAL(wp)  :: ceo_phase_rad(max_sols)
    REAL(wp)  :: field_period(max_sols)
    REAL(wp)  :: time_between_IR_and_XUV_peaks(max_sols)
    REAL(wp)  :: peak_time_of_IR_minus_ramp(max_sols)
    REAL(wp)  :: polarization_offset_angle(max_sols)

    ! All parameters are read in from this one namelist
    namelist /InputData/&
                        & absorb_desired,&
                        & absorption_interval,&
                        & adjust_gs_energy,&
                        & angle_between_pulses_in_deg,&
                        & APT_bursts,&
                        & APT_envelope_power,&
                        & APT_ramp_bursts,&
                        & binary_data_files,&
                        & carrier_envelope_afield,&
                        & ceo_phase_deg,&
                        & ceo_phase_deg_xuv,&
                        & checkpts_per_run,&
                        & coupling_id,&
                        & cross_polarized,&
                        & debug,&
                        & delay_XUV_in_IR_periods,&
                        & deltar,&
                        & dipole_dimensions_desired,&
                        & dipole_format_id,&
                        & dipole_output_desired,&
                        & dipole_velocity_output,&
                        & double_ionization,&
                        & ellipticity,&
                        & ellipticity_xuv,&
                        & envelope_power_primary,&
                        & envelope_power_secondary,&
                        & euler_alpha,&
                        & euler_beta,&
                        & euler_gamma,&
                        & final_t,&
                        & frequency,&
                        & frequency_xuv,&
                        & gs_energy_desired,&
                        & gs_finast,&
                        & Hermitian_Tolerance,&
                        & intensity,&
                        & intensity_xuv,&
                        & i_couple_to_dim,&
                        & keep_checkpoints,&
                        & keep_popL,&
                        & lplusp,&
                        & ML_max,&
                        & molecular_target,&
                        & my_post_dim_lim,&
                        & neigsrem_1st_sym,&
                        & neigsrem_higher_syms,&
                        & no_of_field_confs,&
                        & no_of_omp_threads_inner,&
                        & no_of_omp_threads_outer,&
                        & no_of_pes_per_sector,&
                        & no_of_pes_to_use_double_outer,&
                        & no_of_pes_to_use_inner,&
                        & no_of_pes_to_use_outer,&
                        & periods_of_pulse,&
                        & periods_of_pulse_xuv,&
                        & periods_of_ramp_on,&
                        & periods_of_ramp_on_xuv,&
                        & propagation_order,&
                        & read_initial_wavefunction_from_file,&
                        & reduced_lamax,&
                        & reduced_L_max,&
                        & remove_eigvals_threshold,&
                        & req_array_dim,&
                        & sigma_factor,&
                        & square_double_outer_grid,&
                        & start_factor,&
                        & steps_per_run_approx,&
                        & surf_amp_threshold_value,&
                        & timesteps_per_output,&
                        & time_between_peaks_in_fs,&
                        & timings_desired,&
                        & use_2colour_field,&
                        & use_field_from_file,&
                        & use_pulse_train,&
                        & use_taylor_propagator,&
                        & use_two_electron_we_ham,&
                        & use_two_electron_wp_ham,&
                        & use_two_inner_grid_pts,&
                        & version_root,&
                        & wd_ham_interactions,&
                        & we_ham_interactions,&
                        & window_cutoff,&
                        & window_FWHM,&
                        & window_harmonics,&
                        & wp_ham_interactions,&
                        & write_ground,&
                        & xy_plane_desired,&
                        & x_last_master,&
                        & x_last_others



    CHARACTER(LEN=:), ALLOCATABLE    :: disk_path

CONTAINS

    SUBROUTINE get_disk_path(default_path)

        CHARACTER(LEN=*), INTENT(IN) :: default_path
        CHARACTER(LEN=180)            :: read_path
        INTEGER                      :: disk_path_length
        INTEGER                      :: ierr

        CALL get_environment_variable('disk_path', read_path, disk_path_length, ierr, .true.)

        IF (disk_path_length == 0) THEN
            disk_path = default_path
        ELSE
            ALLOCATE (CHARACTER(disk_path_length) :: disk_path)
            disk_path = read_path(1:disk_path_length)
        END IF

    END SUBROUTINE get_disk_path

!---------------------------------------------------------------------------

    SUBROUTINE set_defaults

        no_of_field_confs        = 1
        molecular_target         = .false.
        double_ionization        = .false.
        ellipticity              = 0.0_wp
        euler_alpha              = 0.0_wp
        euler_beta               = 0.0_wp
        euler_gamma              = 0.0_wp
        frequency                = 1.0_wp
        lplusp                   = 0
        set_ML_max               = .true.
        ML_max                   = -1
        gs_finast                = 1
        adjust_gs_energy         = .false.
        gs_energy_desired        = 0.0_wp
        use_2colour_field        = .false.
        use_pulse_train          = .false.
        use_field_from_file      = .false.
        carrier_envelope_afield  = .false.
        periods_of_ramp_on       = 2.5_wp
        periods_of_pulse         = 5.0_wp
        ceo_phase_deg            = 0.0_wp
        intensity                = 1.0_wp
        ellipticity_XUV          = 0.0_wp
        cross_polarized          = .false.
        frequency_XUV            = 1.0_wp
        periods_of_ramp_on_XUV   = 2.5_wp
        periods_of_pulse_XUV     = 5.0_wp
        ceo_phase_deg_xuv        = 0.0_wp
        intensity_XUV            = 1.0_wp
        time_between_peaks_in_fs = 0.0_wp
        angle_between_pulses_in_deg = 0.0_wp
        envelope_power_primary   = 1
        envelope_power_secondary = 1
        xy_plane_desired         = .false.
        dipole_output_desired    = .false.
        dipole_velocity_output   = .false.
        no_of_pes_to_use_inner   = -1
        no_of_pes_to_use_outer   = -1
        no_of_pes_to_use_double_outer = 0
        no_of_pes_to_use_double_outer   = 0
        no_of_pes_per_sector = 1
        no_of_omp_threads_inner  = 1
        no_of_omp_threads_outer  = 1
        req_array_dim            = 250
        i_couple_to_dim          = 10
        my_post_dim_lim          = 100
        x_last_master            = -1
        x_last_others            = -1
        use_taylor_propagator    = .false.
        propagation_order        = 8
        steps_per_run_approx     = -1
        final_t                  = -1.0_wp
        delta_t                  = 0.01_wp
        deltaR                   = 0.08_wp
        offset_in_stepsizes      = 0.0_wp
        use_two_inner_grid_pts   = .false.
        square_double_outer_grid = .false.
        version_root             = ''
        keep_checkpoints         = .false.
        checkpts_per_run         = 1
        timesteps_per_output     = 20
        timings_desired          = .false.
        remove_eigvals_threshold = .true.
        surf_amp_threshold_value = 1.0_wp
        neigsrem_1st_sym         = 14
        neigsrem_higher_syms     = 20
        wd_ham_interactions      = .true.
        wp_ham_interactions      = .true.
        we_ham_interactions      = .true.
        use_two_electron_we_ham  = .true.
        use_two_electron_wp_ham  = .true.
        reduced_lamax            = -1
        reduced_L_max            = -1
        dipole_format_id         = RMatrixII_format_id
        absorb_desired           = .false.
        sigma_factor             = 0.2_wp
        start_factor             = 0.7_wp
        absorption_interval      = 20
        debug                    = .false.
        coupling_id              = LS_coupling_id
        write_ground             = .true.
        binary_data_files        = .false.
        window_harmonics         = .true.
        read_initial_wavefunction_from_file = .false.
        window_cutoff            = 100.00_wp
        window_FWHM              = 50.00_wp
        Hermitian_Tolerance      = 10e-12_wp
        keep_popL               = .true.
        dipole_dimensions_desired=(/.FALSE.,.FALSE.,.TRUE./)
        APT_bursts               = 0.0_wp
        APT_ramp_bursts               = 0.0_wp
        APT_envelope_power       = 8.0_wp
        delay_XUV_in_IR_periods  = -999

    END SUBROUTINE

!---------------------------------------------------------------------------

    SUBROUTINE read_initial_conditions
    ! Subroutine to read calculation parameters from input.conf

        USE rmt_assert, ONLY: assert

        IMPLICIT NONE

        INTEGER           :: ioerror, numsols
        CHARACTER(LEN=80) :: line

        CALL set_defaults

        ! Try reading pure namelist format
        OPEN (UNIT=10, FILE=disk_path//'input.conf', STATUS='old')
        READ (10, NML=inputdata, IOSTAT=ioerror)
        IF (IOerror/=0) THEN
            BACKSPACE(10)
            READ(10,FMT='(A)') line
            CALL assert(.false.,'Invalid parameter in input.conf: '//TRIM(line))
        END IF
        CLOSE (10)

        CALL assert( (no_of_pes_to_use_inner /= -1) , &
        'Error: there is no no_of_pes_to_use_inner entry in input file')

        CALL assert( (no_of_pes_to_use_outer /= -1) , &
        'Error: there is no no_of_pes_to_use_outer entry in input file')

        CALL assert( (x_last_master /= -1) , &
        'Error: there is no x_last_master entry in input file')

        CALL assert( (x_last_others /= -1) , &
        'Error: there is no x_last_others entry in input file')

        CALL assert( (final_t /= -1) , &
        'Error: there is no final_t entry in input file')

        ! abbreviation
        numsols = no_of_field_confs

        ! Derived parameters
        IF (use_pulse_train) THEN
            ! This makes the total XUV field the same length as the IR. 
            ! We then modulate the field with an envelope function to
            ! produce the pulse train.
            periods_of_pulse_XUV(1:numsols) = (frequency_XUV(1:numsols)/frequency(1:numsols)) * periods_of_pulse(1:numsols)
        END IF

        no_of_pes_to_use = no_of_pes_to_use_inner + no_of_pes_to_use_outer + no_of_pes_to_use_double_outer

        field_period_XUV(1:numsols) = 2.0*pi/frequency_XUV(1:numsols)
        ceo_phase_rad_xuv(1:numsols) = pi*ceo_phase_deg_xuv(1:numsols)/180.0_wp
        ceo_phase_rad(1:numsols) = pi*ceo_phase_deg(1:numsols)/180.0_wp
        field_period(1:numsols) = 2.0*pi/frequency(1:numsols)

        IF (ALL(delay_XUV_in_IR_periods<-998)) THEN ! User has set delay in terms of fs 
            time_between_IR_and_XUV_peaks(1:numsols) = time_between_peaks_in_fs(1:numsols)*41.341373_wp
            peak_time_of_IR_minus_ramp(1:numsols) = &
                periods_of_pulse(1:numsols) / (2.0_wp*frequency(1:numsols)) - &
                periods_of_pulse_XUV(1:numsols)/(frequency_XUV(1:numsols)*2.0_wp)
            delay_XUV_in_IR_periods(1:numsols) = &
                ((time_between_IR_and_XUV_peaks(1:numsols)/(2.0_wp*pi)) + &
                 peak_time_of_IR_minus_ramp(1:numsols)) * frequency(1:numsols)
        END IF

        polarization_offset_angle(1:numsols) = angle_between_pulses_in_deg(1:numsols)*pi/180.0_wp

        WHERE (cross_polarized(1:numsols)) ellipticity(1:numsols) = 0

        CALL assert(.NOT.(ANY(envelope_power_primary(1:numsols) < 1)), "Minimum power of sin^2 envelope is 1.")
        CALL assert(.NOT.(ANY(envelope_power_secondary(1:numsols) < 1)), "Minimum power of sin^2 envelope is 1.")

        IF (ML_max == -1) set_ML_max = .false.

        ! To rotate from the default yz plane to xy plane, the Euler angles should all be set to 90 degrees.
        ! This means ex becomes ez,  ey becomes -ey, ez becomes ex.
        ! So the default polarization vector (z-i*Ellipticity*y) becomes (x+i*Ellipticity*y)
     
        CALL assert(.NOT.(ANY(xy_plane_desired(1:numsols)) .AND. set_ML_max), &
        "Both ML max and xy plane polarization set. Choose only one of these.")

        CALL assert(.NOT.(double_ionization .AND. ML_max /= 0), &
        "For double ionization set ML_max = 0.")

        CALL assert(.NOT.(ANY(xy_plane_desired(1:numsols)) .AND. (GS_finast /= 1)),&
        "xy-plane reduction can only be used for S^e initial state")

        WHERE (xy_plane_desired(1:numsols))
           euler_alpha(1:numsols) = 90
           euler_beta(1:numsols)  = 90
           euler_gamma(1:numsols) = 90
        END WHERE

        CALL assert(ALL(APT_bursts <= periods_of_pulse), "the attosecond pulse train must be shorter&
                                                     &than the fundamental field. I.E you must set&
                                                     &APT_bursts <= periods_of_pulse")

        IF ((reduced_L_max /= -1) .and. (molecular_target)) THEN
            print *, "WARNING: reduced_L_max is only compatible with atomic input data. L_max will not be reduced for this run"
        END IF

        CALL assert((offset_in_stepsizes >= 0.0_wp).AND.(offset_in_stepsizes < 1.0_wp), &
        "Grid offset must be between 0 and 1")

        IF ((offset_in_stepsizes > 0.0_wp) .AND. (molecular_target)) THEN
            print *, "WARNING: Offset outer grid is only compatible with atomic input data. Grid will not be offset for this run"
            offset_in_stepsizes = 0.0_wp
        ELSE IF ((offset_in_stepsizes > 0.0_wp) .AND. (.NOT. use_two_inner_grid_pts)) THEN
            print *, "WARNING: Offset outer grid uses two inner grid points. Setting use_two_inner_grid_pts = .true. now"
            use_two_inner_grid_pts = .TRUE.
        END IF


    END SUBROUTINE read_initial_conditions

END MODULE initial_conditions

!> @defgroup target_info Target Informatiom
!> @defgroup laser_params Laser Parameters
!> @defgroup physics_params Physics Parameters
!> @defgroup discrete_params Discretisation Parameters
!> @defgroup parallel_params Parallelisation Parameters
!> @defgroup double_params Double Ionisation Parameters
!> @defgroup io_params I/O Parameters
!> @defgroup absorb Absorbing Boundary Parameters

!> @page inputs RMT: Input
!!
!! @brief Description of input files and parameters for RMT
!!
!! @tableofcontents
!!
!!
!!
!! @section data_files Data files
!!
!! The input data containing information about the ground and excited states of the
!! target system, the dipole couplings and the spline basis is produced by one of
!! three different software packages.  Non-relativistic atomic data can be provided
!! by either the R-matrix I [1](http://connorb.freeshell.org) or R-matrix II
!! packages [2](https://gitlab.com/Uk-amor/RMT/rmatrixII).  Semi-relativistic data
!! is produced by R-matrix I, and molecular data by UKRMol+
!! [3](https://gitlab.com/Uk-amor/UKRMol), all of which can be obtained, along with
!! documentation, at the repositories linked.
!!
!! The input files required are slightly different depending on the package used.
!! For molecular calculations, all relevant input data is stored in a single file
!! `molecular_data`.  For both the atomic cases, the Hamiltonian data is stored in
!! file `H` and the spline basis information in files `Splinedata` and
!! `Splinewaves`.  The dipole information is stored in files `d` and `d00` for
!! R-matrix II.  For R-matrix I, an individual file is used for each dipole
!! coupling block.  Hence the header information is stored in file `D00` and the
!! individual dipole files are of the form `D###` (e.g. `D001`, `D002` etc.)
!!
!!
!! @section input_params Input parameters
!!
!! RMT reads input from the file ‘input.conf’ which controls the setup of the laser
!! pulse(s), the parallel structure of the calculation and various other
!! calculation specific parameters. 
!!
!! Lines in the input file may be commented with the hash symbol `#`. All
!! inputs begin with the option name, then
!! an equals sign, followed by the value of the option. for example:
!!
!! `Frequency  = 2.0`
!!
!! true/false options can be written with or without the fortran dots, for
!! example
!!
!! `USE_2Colour_Field          = false`
!!
!! `Keep_CheckPoints           =.false.`
!!
!! are both valid.
!!
!! all spaces are stripped from the file, but tab characters will break the
!! input.
!!
!! There is no need to include quotation marks for strings e.g:
!!
!! `Version_root               = He_w390_I101014_05`
!!
!! @subsection opt_params Input Parameters
!! Some parameters are required (REQUIRED), some will need to be set for most
!! practical calculations, and some should be left alone (EXPERT), for all except
!! expert users (e.g for tuning communications). If a parameter is required, or
!! is only useful for expert users, this is marked in the description
!!
!! * @ref target_info "Target Informtaion"
!!
!! * @ref laser_params "Laser Parameters"
!!
!! * @ref io_params "I/O Parameters"
!!
!! * @ref discrete_params "Discretisation Parameters"
!!
!! * @ref physics_params "Physics Parameters"
!!
!! * @ref parallel_params "Parallelisation Parameters"
!!
!! * @ref double_params "Double Ionisation Parameters"
!!
!! * @ref absorb "Absorbing Boundary Parameters"
!!
!! @paragraph disk_path Disk Path
!!
!! By default, the executable will look in the run-time directory
!! for the input files, `input.conf`, `H`, `d00` etc.
!! Should you want to place the input files in a different
!! location, for instance for a batch job, the path to the input
!! files should be set as the environment variable `disk_path`.
!!
!! e.g. batch job script for queuing system:
!!
!! ```
!! >> ls
!!
!!    submit.pbs run_dir_1 run_dir_2
!!
!! >> ls run_dir*
!!
!!    run_dir_1:
!!
!!    d d00 data ground H input.conf rmt.x Splinedata Splinewaves state
!!
!!    run_dir_2:
!!
!!    d d00 data ground H input.conf rmt.x Splinedata Splinewaves state
!!
!! >> tail submit.pbs
!!
!!    # variable PBS_ARRAY_INDEX loops over values 1,2
!!
!!    export disk_path=$PWD/run_dir_$PBS_ARRAY_INDEX
!!
!!    mpirun -n 32 $disk_path/rmt.x > $disk_path/rmt.log
!!
!! ```
!! \addtogroup parallel_params Size of the outer region
!! @paragraph parallel_params Size of the outer region
!!
!! Determining the size of the outer region is non-trivial. This is because of
!! the various arrangements of processors that can be employed in the @ref
!! outer_para "three layers of parallelism". The formula for computing the extent of
!! the outer region is
!!
!! `r_outer` = `r_inner` + `deltaR` * [`x_last_master` + `x_last_others` * (`No_of_PEs_to_use_outer` - 1)/`No_of_PEs_per_sector`)]
!! 
!! There is a python utility `RMT_OR_size` which computes the outer region size using a valid input.conf file (and, optionally, H file): if the utility suite is installed simply execute 
!!
!! `RMT_OR_size <path to calculation directory` 
!!
!! or if not, 
!!
!! `python <path to RMT repo>/source/rmt_utilites/OR_size.py <path to calculation directory>`.
!!
!! \addtogroup absorb Absorbing Boundary
!! @paragraph abs_bound Absorbing boundary
!!
!! By default, no absorbing boundary is enabled in the outer
!! region. This means if your outer region is not sufficiently
!! large the outgoing wavefunction will reflect from the boundary
!! and create spurious oscillations in the results.
!! 
!!
!!    `Absorb_Desired` (Logical- set true to enable)
!!
!!    `Start_Factor`. `Start_Factor=0.7` would start the absorbing
!!    boundary 70% in to the outer region. 
!!
!!    `Sigma_Factor` sets the severity of the absorption. the larger
!!    this is, the gentler the absorption.
!!
!!    The equation for the mask function is given by
!!    \f{eqnarray*}{
!!    M(x) & = 1 \quad \mbox{ for } x< x_s \\ \\
!!    M(x) &= exp\left[-\left(\frac{x-x_s}{\sigma}\right)^2\right]
!!    \quad \mbox{ for } x_s \le x \le x_l
!!    \f}
!!
!!
!! where \f$x_l\f$ is the outermost point in the outer
!! region, \f$x_s =\f$
!! `start_factor`\f$\;\times\; x_l\f$ and
!! \f$\sigma=\frac{x_l}{2}\;\times\;\f$`sigma_factor`
!!
!! The mask function for an outer region of 1000 a.u. is shown for a
!! variety of parameters below. Note that the outermost point is 1020
!! a.u. as the inner region is 20 a.u.
!!
!! @image html absorb.png
!! @image latex absorb.png
