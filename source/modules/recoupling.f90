MODULE recoupling
    USE precisn, ONLY: wp
    USE modChannel, ONLY: Channel, &
                          jk_Channel_ID, &
                          DOE_Channel_ID, &
                          LS_Channel_ID, &
                          Uncoupled_Target_ID, &
                          SetupChannel
    IMPLICIT NONE

    PRIVATE

    PUBLIC :: Recouple, GetQnums

CONTAINS

    SUBROUTINE Recouple(ChannelsIn, wv)
        TYPE(Channel), INTENT(INOUT), ALLOCATABLE:: ChannelsIn(:)
        COMPLEX(wp), ALLOCATABLE, INTENT(INOUT) :: wv(:, :, :)

        SELECT CASE (ChannelsIn(1)%Species)
            CASE (jK_Channel_ID)
                CALL RecoupleJKtoDOE(ChannelsIn, wv)
            CASE (LS_Channel_ID)
                CALL RecoupleLStoUncoupled(ChannelsIn, wv)
        END SELECT

    END SUBROUTINE Recouple

    SUBROUTINE GetQnums(ChannelIn, TargetL, TargetML, elecMS, elecL, elecML, coupl)
        USE angular_momentum, ONLY: cg
        USE modChannel, ONLY: Channel, &
                              DOE_Channel_ID, &
                              LS_Channel_ID, &
                              Uncoupled_Target_ID
        TYPE(Channel), INTENT(IN) :: ChannelIn
        INTEGER, INTENT(IN) :: TargetL, TargetML, elecMS
        INTEGER, INTENT(OUT) :: elecL, elecML
        REAL(wp), INTENT(OUT) :: coupl
        INTEGER :: TotalL, TotalML

        coupl = 0.0_wp
        SELECT CASE (ChannelIn%Species)
        CASE (LS_Channel_ID)
            elecL = ChannelIn%QNumber(3)
            TotalL = ChannelIn%QNumber(4)
            TotalML = ChannelIn%QNumber(5)
            elecML = TotalML - TargetML
            IF (ABS(elecML) <= elecL) THEN
                coupl = cg(TargetL, elecL, TotalL, TargetML, elecML, TotalML)
            ELSE
                coupl = 0.0_wp
            END IF

        CASE (DOE_Channel_ID)
            elecL = ChannelIn%QNumber(4)
            elecML = ChannelIn%QNumber(5)
            IF (ChannelIn%QNumber(7) == elecMS) THEN
                IF (channelIN%QNumber(3) == TargetML) THEN
                    IF (channelIN%QNumber(2) == TargetL) THEN
                        coupl = 1.0_wp
                    END IF
                END IF
            END IF

        CASE (Uncoupled_Target_ID)
            elecL = ChannelIn%QNumber(4)
            elecML = ChannelIn%QNumber(5)
            IF (channelIN%QNumber(3) == TargetML) THEN
                IF (channelIN%QNumber(2) == TargetL) THEN
                    coupl = 1.0_wp
                END IF
            END IF
        END SELECT

    END SUBROUTINE GetQnums

    SUBROUTINE RecoupleJKtoDOE(ChannelsJK, wv)
        USE modChannel, ONLY: Channel, &
                              AttachWfnToChannel, &
                              DetachWfnFromChannel

        TYPE(Channel), INTENT(INOUT), ALLOCATABLE:: ChannelsJK(:)
        COMPLEX(wp), ALLOCATABLE, INTENT(INOUT) :: wv(:, :, :)

        TYPE(Channel), ALLOCATABLE :: ChannelsUncoupled(:)
        REAL(wp), ALLOCATABLE :: RecouplingMatrix(:, :)

        ALLOCATE (ChannelsUncoupled(0))

        CALL MakeDecoupledOuterElectronChannels(ChannelsJK, ChannelsUncoupled)
        CALL MakeRecouplingMatrixjKtoDOE(ChannelsJK, ChannelsUncoupled, RecouplingMatrix)
        CALL AttachWfnToChannel(ChannelsJK, wv)
        CALL RecoupleChannels(ChannelsJK, ChannelsUncoupled, RecouplingMatrix)
        CALL DetachWfnFromChannel(ChannelsJK, wv)

    END SUBROUTINE RecoupleJKtoDOE

    SUBROUTINE RecoupleLStoUncoupled(ChannelsLS, wv)
        USE modChannel, ONLY: Channel, &
                              AttachWfnToChannel, &
                              DetachWfnFromChannel

        TYPE(Channel), INTENT(INOUT), ALLOCATABLE:: ChannelsLS(:)
        COMPLEX(wp), ALLOCATABLE, INTENT(INOUT) :: wv(:, :, :)

        TYPE(Channel), ALLOCATABLE :: ChannelsUncoupled(:)
        REAL(wp), ALLOCATABLE :: RecouplingMatrix(:, :)

        ALLOCATE (ChannelsUncoupled(0))

        CALL MakeUncoupledChannels(ChannelsLS, ChannelsUncoupled)
        CALL MakeRecouplingMatrixLStoUncoupled(ChannelsLS, ChannelsUncoupled, RecouplingMatrix)
        CALL AttachWfnToChannel(ChannelsLS, wv)
        CALL RecoupleChannels(ChannelsLS, ChannelsUncoupled, RecouplingMatrix)
        CALL DetachWfnFromChannel(ChannelsLS, wv)

    END SUBROUTINE RecoupleLStoUncoupled

    SUBROUTINE RecoupleChannels(ChannelsIn, ChannelsOut, RecouplingMatrix)
        USE modChannel, ONLY: AllocateWfns
        TYPE(Channel), INTENT(INOUT), ALLOCATABLE:: ChannelsIn(:)
        TYPE(Channel), INTENT(INOUT), ALLOCATABLE:: ChannelsOut(:)
        REAL(wp), ALLOCATABLE, INTENT(IN):: RecouplingMatrix(:, :)
        INTEGER:: Number_of_Channels_In
        INTEGER:: Number_of_Channels_Out
        INTEGER:: Channel_In_Counter
        INTEGER:: Channel_Out_Counter
        INTEGER:: npts, nsols

        Number_of_Channels_In = UBOUND(ChannelsIn, 1)
        Number_of_Channels_Out = UBOUND(ChannelsOut, 1)

        ASSOCIATE (wfn => ChannelsIn(1)%Wavefunction)
            npts = UBOUND(wfn, 1)
            nsols = UBOUND(wfn, 2)
            CALL AllocateWfns(ChannelsOut, npts, nsols)
        END ASSOCIATE

        DO Channel_In_Counter = 1, Number_of_Channels_In
            DO Channel_Out_Counter = 1, Number_of_Channels_Out

                ASSOCIATE (coeff => RecouplingMatrix(Channel_In_Counter, Channel_Out_Counter))

                    IF (ABS(coeff) .GT. 0.000001) THEN

                        ChannelsOut(Channel_Out_Counter)%Wavefunction(:, :) = &
                            ChannelsOut(Channel_Out_Counter)%Wavefunction(:, :) + &
                            coeff*ChannelsIn(Channel_In_Counter)%Wavefunction(:, :)

                    END IF
                END ASSOCIATE
            END DO
        END DO

        DEALLOCATE (ChannelsIn)
        ALLOCATE (ChannelsIn(Number_of_Channels_Out))
        ChannelsIn = ChannelsOut

    END SUBROUTINE RecoupleChannels

    SUBROUTINE MakeUncoupledChannels(LSChannels, UncoupledChannels)
        USE modChannel, ONLY: ChannelIncluded, Channel, AppendChannel
        TYPE(Channel), INTENT(IN), ALLOCATABLE:: LSChannels(:)
        TYPE(Channel), INTENT(INOUT), ALLOCATABLE:: UncoupledChannels(:)
        TYPE(Channel):: TempUncoupledChannel

        INTEGER:: Number_of_LS_Channels
        INTEGER:: targetML
        INTEGER:: elecML
        INTEGER:: targetMS
        INTEGER:: elecMS
        INTEGER:: LS_Channel_Counter

        CALL SetupChannel(TempUncoupledChannel, Uncoupled_Target_ID)
        Number_of_LS_Channels = UBOUND(LSChannels, 1)
        DO LS_Channel_Counter = 1, Number_of_LS_Channels

            ASSOCIATE (targetN => LSChannels(LS_Channel_Counter)%QNumber(1), &
                       targetL => LSChannels(LS_Channel_Counter)%QNumber(2), &
                       elecL => LSChannels(LS_Channel_Counter)%QNumber(3), &
                       totalML => LSChannels(LS_Channel_Counter)%QNumber(5), &
                       targetS => LSChannels(LS_Channel_Counter)%QNumber(6), &
                       totalMS => LSChannels(LS_Channel_Counter)%QNumber(8), &
                       parity => LSChannels(LS_Channel_Counter)%QNumber(9))

                TempUncoupledChannel%QNumber(1) = targetN
                TempUncoupledChannel%QNumber(2) = targetL
                TempUncoupledChannel%QNumber(4) = elecL
                TempUncoupledChannel%QNumber(6) = targetS
                TempUncoupledChannel%QNumber(9) = parity

                DO targetML = -targetL, targetL
                    elecML = totalML - targetML
                    TempUncoupledChannel%QNumber(3) = targetML
                    TempUncoupledChannel%QNumber(5) = elecML

                    DO targetMS = -targetS, targetS, 2
                        elecMS = totalMS - targetMS

                        TempUncoupledChannel%QNumber(7) = targetMS
                        TempUncoupledChannel%QNumber(8) = elecMS

                        IF (ABS(elecML) .LE. elecL) THEN
                        IF (ABS(elecMS) .LE. 1) THEN
                            IF (.NOT. ChannelIncluded(UncoupledChannels, TempUncoupledChannel)) THEN
                                CALL AppendChannel(UncoupledChannels, TempUncoupledChannel)
                            END IF
                        END IF
                        END IF
                    END DO
                END DO
            END ASSOCIATE

        END DO

    END SUBROUTINE MakeUncoupledChannels

    SUBROUTINE MakeDecoupledOuterElectronChannels(JKChannels, DOEChannels)
        USE modChannel, ONLY: ChannelIncluded, Channel, AppendChannel
        TYPE(Channel), INTENT(INOUT), ALLOCATABLE:: DOEChannels(:)
        TYPE(Channel), INTENT(IN), ALLOCATABLE:: JKChannels(:)
        TYPE(Channel):: TempDOEChannel

        INTEGER:: Number_of_JK_Channels
        INTEGER:: JK_Channel_Counter

        INTEGER:: TargetN
        INTEGER:: TargetJ
        INTEGER:: ElecL
        INTEGER:: ElecS
        INTEGER:: elecML
        INTEGER:: elecMS
        INTEGER:: TargetMJ
        INTEGER:: Parity
        INTEGER:: totalK

        INTEGER:: totalMJ

        CALL SetupChannel(TempDOEChannel, DOE_Channel_ID)
        Number_of_JK_Channels = UBOUND(JKChannels, 1)
        DO JK_Channel_Counter = 1, Number_of_JK_Channels
            ASSOCIATE (chan => JKChannels(JK_Channel_Counter))

                TargetN = chan%QNumber(1)
                TargetJ = chan%QNumber(2)
                ElecL = chan%QNumber(3)
                totalK = chan%QNumber(4)
                totalMJ = chan%QNumber(6)
                Parity = chan%QNumber(7)
                ElecS = 1

                DO elecMS = -1, 1, 2
                    DO elecML = -ElecL, ElecL
                        TargetMJ = totalMJ - elecMS - 2*elecML

                        IF (ABS(TargetMJ) <= TargetJ) THEN
                        IF (2*ElecL + TargetJ >= totalK) THEN
                        IF (ABS(2*ElecL - TargetJ) <= totalK) THEN
                            TempDOEChannel%QNumber(1) = TargetN
                            TempDOEChannel%QNumber(2) = TargetJ
                            TempDOEChannel%QNumber(3) = TargetMJ
                            TempDOEChannel%QNumber(4) = ElecL
                            TempDOEChannel%QNumber(5) = elecML
                            TempDOEChannel%QNumber(6) = ElecS
                            TempDOEChannel%QNumber(7) = elecMS
                            TempDOEChannel%QNumber(8) = Parity

                            IF (.NOT. ChannelIncluded(DOEChannels, TempDOEChannel)) THEN
                                CALL AppendChannel(DOEChannels, TempDOEChannel)
                            END IF
                        END IF
                        END IF
                        END IF
                    END DO
                END DO
            END ASSOCIATE
        END DO

    END SUBROUTINE MakeDecoupledOuterElectronChannels

    SUBROUTINE MakeRecouplingMatrixLStoUncoupled(LSChannels, UncoupledChannels, RecouplingMatrix)
        USE angular_momentum, ONLY: dcg, cg
        TYPE(Channel), INTENT(IN), ALLOCATABLE:: LSChannels(:)
        TYPE(Channel), INTENT(IN), ALLOCATABLE:: UncoupledChannels(:)
        REAL(wp), INTENT(OUT), ALLOCATABLE:: RecouplingMatrix(:, :)

        INTEGER:: Number_of_LS_Channels
        INTEGER:: Number_of_Uncoupled_Channels

        INTEGER:: LS_Channel_Counter
        INTEGER:: Uncoupled_Channel_Counter

        REAL(wp):: Coeff

        INTEGER:: elecS
        INTEGER:: elecMS
        INTEGER:: targetMS
        INTEGER:: totalMS
        INTEGER:: totalS
        INTEGER:: targetS_LS
        INTEGER:: targetS_UN
        INTEGER:: totalL
        INTEGER:: elecML
        INTEGER:: totalML
        INTEGER:: TargetML
        INTEGER:: Parity_UN
        INTEGER:: Parity_LS
        INTEGER:: targetL_UN
        INTEGER:: targetL_LS
        INTEGER:: targetN_UN
        INTEGER:: targetN_LS
        INTEGER:: elecL_UN
        INTEGER:: elecL_LS

        Number_of_LS_Channels = UBOUND(LSChannels, 1)
        Number_of_Uncoupled_Channels = UBOUND(UncoupledChannels, 1)

        ALLOCATE (RecouplingMatrix(Number_of_LS_Channels, Number_of_Uncoupled_Channels))
        RecouplingMatrix = 0.0_wp

        DO LS_Channel_Counter = 1, Number_of_LS_Channels
            DO Uncoupled_Channel_Counter = 1, Number_of_Uncoupled_Channels
                ASSOCIATE (chanLS => LSChannels(LS_Channel_Counter), &
                           chanUN => UncoupledChannels(Uncoupled_Channel_Counter))

                    elecS = 1
                    totalS = chanLS%QNumber(7)
                    targetS_LS = chanLS%QNumber(6)
                    targetS_UN = chanUN%QNumber(6)

                    elecMS = chanUN%QNumber(8)
                    TargetMS = chanUN%QNumber(7)
                    TotalMS = chanLS%QNumber(8)

                    TargetML = chanUN%QNumber(3)
                    elecML = chanUN%QNumber(5)
                    totalML = chanLS%QNumber(5)

                    totalL = chanLS%QNumber(4)
                    elecL_UN = chanUN%QNumber(4)
                    elecL_LS = chanLS%QNumber(3)
                    TargetL_UN = chanUN%QNumber(2)
                    TargetL_LS = chanLS%QNumber(2)

                    Parity_UN = chanUN%QNumber(9)
                    Parity_LS = chanLS%QNumber(9)

                    TargetN_UN = chanUN%QNumber(1)
                    TargetN_LS = chanLS%QNumber(1)

                    !Check Target Number
                    IF (TargetN_LS == TargetN_UN) THEN
                        !Check Target J
                        IF (TargetL_LS == TargetL_UN) THEN
                            !Check Elecl
                            IF (elecL_LS == elecL_UN) THEN
                                !Check Parity
                                IF (Parity_LS == Parity_UN) THEN
                                    !Check ML = melecl+mtargetl
                                    IF (totalML == (elecML + targetML)) THEN
                                        !Check Target Spin
                                        IF (TargetS_LS == TargetS_UN) THEN
                                            !DONE UP TO HERE
                                            !Check Ms = melecs+mtargets
                                            IF (totalMS == (elecMS + targetMS)) THEN

                                                Coeff = cg(TargetL_LS, elecL_LS, totalL, TargetML, elecMl, totalML)* &
                                                        dcg(TargetS_LS, elecS, totalS, targetMS, elecMS, totalMS)
                                                RecouplingMatrix(LS_Channel_Counter, Uncoupled_Channel_Counter) = &
                                                    RecouplingMatrix(LS_Channel_Counter, Uncoupled_Channel_Counter) + Coeff
                                            END IF
                                        END IF
                                    END IF
                                END IF
                            END IF
                        END IF
                    END IF

                END ASSOCIATE

            END DO
        END DO
    END SUBROUTINE MakeRecouplingMatrixLStoUncoupled

    SUBROUTINE MakeRecouplingMatrixjKtoDOE(JKChannels, DOEChannels, RecouplingMatrix)
        USE modChannel, ONLY: satisfies_triangle_rule
        USE angular_momentum, ONLY: dcg
        TYPE(Channel), INTENT(IN), ALLOCATABLE:: JKChannels(:)
        TYPE(Channel), INTENT(IN), ALLOCATABLE:: DOEChannels(:)
        REAL(wp), INTENT(OUT), ALLOCATABLE:: RecouplingMatrix(:, :)

        INTEGER:: Number_of_JK_Channels
        INTEGER:: Number_of_DOE_Channels

        INTEGER:: JK_Channel_Counter
        INTEGER:: DOE_Channel_Counter

        REAL(wp):: Coeff

        INTEGER:: elecML
        INTEGER:: S
        INTEGER:: MS
        INTEGER:: J
        INTEGER:: MJ
        INTEGER:: K
        INTEGER:: MK
        INTEGER:: TargetMJ
        INTEGER:: Parity_DOE
        INTEGER:: Parity_jK
        INTEGER:: targetj_DOE
        INTEGER:: targetj_jK
        INTEGER:: targetN_DOE
        INTEGER:: targetN_jK
        INTEGER:: elecL_DOE
        INTEGER:: elecL_jK

        Number_of_JK_Channels = UBOUND(JKChannels, 1)
        Number_of_DOE_Channels = UBOUND(DOEChannels, 1)

        ALLOCATE (RecouplingMatrix(Number_of_JK_Channels, Number_of_DOE_Channels))
        RecouplingMatrix = 0.0_wp

        DO JK_Channel_Counter = 1, Number_of_JK_Channels
            DO DOE_Channel_Counter = 1, Number_of_DOE_Channels
                ASSOCIATE (chanJK => JKChannels(JK_Channel_Counter), &
                           chanDOE => DOEChannels(DOE_Channel_Counter))

                    K = chanJK%QNumber(4)
                    S = 1
                    MS = chanDOE%QNumber(7)
                    TargetMJ = chanDOE%QNumber(3)
                    elecML = chanDOE%QNumber(5)
                    J = chanJK%QNumber(5)
                    MJ = chanJK%QNumber(6)
                    MK = TargetMJ + 2*elecML

                    Parity_DOE = chanDOE%QNumber(8)
                    Parity_jK = chanJK%QNumber(7)

                    elecL_DOE = chanDOE%QNumber(4)
                    elecL_jK = chanJK%QNumber(3)

                    TargetN_DOE = chanDOE%QNumber(1)
                    TargetN_jK = chanJK%QNumber(1)

                    Targetj_DOE = chanDOE%QNumber(2)
                    Targetj_jK = chanJK%QNumber(2)

                    !Check Target Number
                    IF (TargetN_jK == TargetN_DOE) THEN
                        !Check Target J
                        IF (Targetj_jK == Targetj_DOE) THEN
                            !Check Elecl
                            IF (elecL_jK == elecL_DOE) THEN
                                !Check Parity
                                IF (Parity_jK == Parity_DOE) THEN
                                    !Check mJ = ml+ms+mj
                                    IF (MJ == (2*elecML + MS + TargetMJ)) THEN
                                        !Check K Triangle Rule
                                        IF (satisfies_triangle_rule(K, targetj_jK, elecL_jk)) THEN

                                            Coeff = dcg(K, S, J, MK, MS, MJ)* &
                                                    dcg(Targetj_jK, 2*elecL_jK, K, TargetMJ, 2*elecML, MK)
                                            RecouplingMatrix(JK_Channel_Counter, DOE_Channel_Counter) = &
                                                Coeff
                                        END IF
                                    END IF
                                END IF
                            END IF
                        END IF
                    END IF

                END ASSOCIATE
            END DO
        END DO
    END SUBROUTINE MakeRecouplingMatrixjKtoDOE

END MODULE recoupling