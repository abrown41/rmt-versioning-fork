! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles the dependencies of the main program (to keep main program clean).
MODULE tdse_dependencies
 
    USE precisn,               ONLY: wp
   
    USE electric_field,        ONLY: get_e_pulse
  
    USE stages,                ONLY: stage_first,&
                                     timeindex_first
  
    USE readhd,                ONLY: max_L_block_size
  
    USE initial_conditions,    ONLY: deltaR,&
                                     no_of_field_confs, &
                                     checkpts_per_run, &
                                     my_post_dim_lim
  
    USE calculation_parameters,ONLY: steps_per_checkpt,&
                                     delta_t
  
    USE wavefunction,          ONLY: data_1st,&
                                     update_wavefunction_module
    
    USE kernel,                ONLY: propagation_loop
  
    USE initialise,            ONLY: initialise_everything
  
    USE finalise,              ONLY: end_program
  
    USE checkpoint,            ONLY: checkpoint_writes

END MODULE tdse_dependencies
