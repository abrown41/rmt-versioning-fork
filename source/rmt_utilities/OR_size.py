"""
Utility for computing the size of the outer region for a given input.conf
"""


def read_command_line():
    from argparse import ArgumentParser as AP
    parser = AP()
    parser.add_argument('file', nargs="?", help="optional: directory containing rmt\
                        calculation", default=".")
    return parser


def get_OR_size(calc, hfile):
    """
    provided an RMT calculation object, and an H file, return the
    extent of the outer region for that calculation.

    Parameters
    ----------
    calc : rmtutilities.RMTCalc
        RMT calculation object
    Hfile : rmtutitilites.dataobjects.Hfile
        RMT Hfile object

    Returns
    -------
    extent : float
        size of the outer region
    """
    ri = hfile.rmatr
    xo = calc.config['x_last_others']
    xm = calc.config['x_last_master']
    np = calc.config['no_of_pes_to_use_outer']
    try:
        pps = calc.config['no_of_pes_per_sector']
    except KeyError:
        pps = 1
    try:
        dR = calc.config['deltar']
    except KeyError:
        dR = 0.08

    return ri + dR * (xm + xo * ((np-1)/pps))


def main():
    from rmt_utilities.dataobjects import Hfile
    from rmt_utilities.rmtutil import RMTCalc
    args = read_command_line().parse_args()
    calc = RMTCalc(args.file)
    hfile = Hfile(args.file)

    print(get_OR_size(calc, hfile))


if __name__ == '__main__':
    main()
