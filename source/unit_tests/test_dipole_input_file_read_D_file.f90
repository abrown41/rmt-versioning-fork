! Copyright 2023
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

MODULE test_dipole_input_file_read_D_file
    USE ISO_FORTRAN_ENV, ONLY: INT32, REAL64    !  Use standard fortran library  *include info*

    USE testdrive, ONLY: check, error_type, new_unittest, test_failed, unittest_type

    USE testing_utilities, ONLY: compare, &
                                data_file, &
                                format_context, &
                                int_to_char, &
                                range, &
                                subarray, &
                                subarray_integer_int32, &
                                subarray_real_real64

    USE initial_conditions, ONLY: debug

    USE precisn, ONLY: wp
    USE dipole_input_file, ONLY: D_file, dipole_block, read_D_file2

    IMPLICIT NONE(TYPE, EXTERNAL)

    INTERFACE check
        MODULE PROCEDURE check_d_file
    END INTERFACE

!> \brief The expected derived type based on the format of the D file derived type from `dipole_input_file`
!>
!> The structure of this type maps almost 1:1 with the structure of `dipole_input_file therefore the structure of D file itself. 
!> The structure is defined in terms of specified-width numeric types (i.e. `int32` and `real64`) because it is an external 
!> filetype and setting `wp` to a different width shouldn't affect the way the file is read.
!>
!> The names of the fields match what is currently being used in the code.
!>
!> # D File Format
!>
!> The 2 files each containing a series of [_Unformatted I/O Records_][scipy-FortranFile]. The format of these records is compiler-and
!> machine-dependent, although most compilers will use a `<record bytes><record data><record bytes>` format and most machines
!> will be little-endian and use IEEE 754 floating point numbers.
!>
!> The first file contains 2 32-bit integers followed by 2 1D arrays both of which are 64-bit real arrays.
!> 
!> The final record in this file defines the D block described as "blocks"
!>
!> The second file contains 13 32-bit integers and then 2 \f$3 \times n\f$ matrix, with \f$n\f$ specified by the integers "nev" & "nod".
!>
!> Lastly there are 2 more 32-bit integers.
!>
!> [scipy-FortranFile]: https://docs.scipy.org/doc/scipy-1.9.3/reference/generated/scipy.io.FortranFile.html

    TYPE expected_D_file
        INTEGER(INT32):: ntarg
        INTEGER(INT32):: fintr
        REAL(REAL64), ALLOCATABLE :: crlv(:, :)
        REAL(REAL64), ALLOCATABLE :: crlv_v(:, :)
        TYPE(expected_dipole_block), DIMENSION(:), ALLOCATABLE:: blocks
    END TYPE

    TYPE expected_dipole_block
        INTEGER(INT32):: noterm, isi, inch, jsi, jnch
        INTEGER(INT32):: nev, nod
        INTEGER(INT32):: lgsf, lglf, lgpf, lgsi, lgli, lgpi
        TYPE(subarray_real_real64) :: dipsto
        TYPE(subarray_real_real64) :: dipsto_v
    END TYPE

    PRIVATE
    PUBLIC :: collect_dipole_input_file_read_D_file   !  name a public subroutine that collects the information on all tests preformed
CONTAINS

!> \brief Collection of tests performed on `dipole_input_file :: read_D_file2`
!> 
!> \param[in] testsuite List of tests.

    SUBROUTINE collect_dipole_input_file_read_D_file(testsuite)   !  This subroutine will collect the outcomes of tests performed
        TYPE(unittest_type), DIMENSION(:), ALLOCATABLE, INTENT(OUT) :: testsuite    ! Type of unit test type with member as initialised as testsuite

        !  testsuite is a list of each test performed, it must begin and end in an "&"
        testsuite = [ new_unittest("atomic::small::Ne_4cores", test_atomic_small_Ne_4cores), &
                      new_unittest("atomic::small::ar+", test_atomic_small_arx), &
                      new_unittest("atomic::small::argon", test_atomic_small_argon), &
                      new_unittest("atomic::small::helium", test_atomic_small_helium)]      

    END SUBROUTINE

    !  Subroutine that returns an error 
    SUBROUTINE check_d_file(error, &
                            actual_ntarg, &
                            actual_fintr, &
                            actual_crlv, &
                            actual_crlv_v, &
                            actual_dipsto, &
                            actual_dipsto_v, &
                            expected, &
                            nstmx, &
                            loc_of_blocks) 
        TYPE(error_type), ALLOCATABLE, INTENT(OUT) :: error
        TYPE(expected_D_file), INTENT(in) :: expected

        CHARACTER(len=:), ALLOCATABLE :: context

        INTEGER, DIMENSION(:), ALLOCATABLE :: expected_shape
        INTEGER :: i
        INTEGER, INTENT(INOUT) :: actual_ntarg
        INTEGER, INTENT(INOUT) :: actual_fintr
        INTEGER, INTENT(IN) :: nstmx
        INTEGER, INTENT(IN)                  :: loc_of_blocks(:)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_crlv(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_crlv_v(:, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_dipsto(:, :, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_dipsto_v(:, :, :)

        context = ""

        IF (actual_ntarg /= expected%ntarg) THEN
            CALL format_context(context, "ntarg", actual_ntarg, expected%ntarg)
        END IF

        IF (actual_ntarg /= expected%ntarg) THEN
            CALL format_context(context, "ntarg", actual_ntarg, expected%ntarg)
        END IF

        IF (.NOT. compare(actual_crlv, expected%crlv)) THEN
            CALL format_context(context, "crlv", actual_crlv, expected%crlv)
        END IF

        IF (.NOT. compare(actual_crlv_v, expected%crlv_v)) THEN
            CALL format_context(context, "crlv_v", actual_crlv_v, expected%crlv_v)
        END IF

        expected_shape = [actual_ntarg, actual_ntarg]
        IF(.NOT. compare(SHAPE(actual_crlv), expected_shape)) THEN
            CALL format_context(context, "shape(crlv)", SHAPE(actual_crlv), expected_shape)
        END IF

        expected_shape = [actual_ntarg, actual_ntarg]
        IF(.NOT. compare(SHAPE(actual_crlv_v), expected_shape)) THEN
            CALL format_context(context, "shape(crlv_v)", SHAPE(actual_crlv_v), expected_shape)
        END IF

        IF (actual_fintr /= expected%fintr) THEN
            CALL format_context(context, "fintr", actual_fintr, expected%fintr)
        END IF 

        DO i = 1, SIZE(loc_of_blocks)
            CALL check_dipole_block(context, &
                                    loc_of_blocks(i), &
                                    actual_dipsto, &
                                    actual_dipsto_v, &
                                    expected%blocks(i), &
                                    nstmx, &
                                    actual_fintr)
        END DO

        IF (context /= "") THEN
            CALL test_failed(error, "D file data set incorrectly!", context)
        END IF

    END SUBROUTINE

    !  Subroutine that compares to numbers that are passed into it's arguements (actual and expected) and outputs error if they are not the same
    SUBROUTINE check_dipole_block(context, index, actual_dipsto, actual_dipsto_v, expected, nstmx, fintr)
        TYPE(expected_dipole_block), INTENT(in) :: expected
        INTEGER, INTENT(IN) :: index
        INTEGER, INTENT(IN) :: nstmx
        INTEGER, INTENT(IN) :: fintr
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_dipsto(:, :, :)
        REAL(wp), ALLOCATABLE, INTENT(INOUT) :: actual_dipsto_v(:, :, :)

        CHARACTER(len=:), ALLOCATABLE :: context, prefix
        INTEGER, DIMENSION(:), ALLOCATABLE :: expected_shape

        prefix = "dipole_block("//int_to_char(index)//")%"
        !  Compare is another function from testing_utilities which compares two values and provides an error should they not be the same
        
        IF (.NOT. compare(actual_dipsto, expected%dipsto)) THEN
            CALL format_context(context, "dipsto", actual_dipsto, expected%dipsto)
        END IF

        IF (.NOT. compare(actual_dipsto_v, expected%dipsto_v)) THEN
            CALL format_context(context, "dipsto_v", actual_dipsto_v, expected%dipsto_v)
        END IF

        expected_shape = [nstmx, nstmx, fintr]
        IF(.NOT. compare(SHAPE(actual_dipsto), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%dipsto)", SHAPE(actual_dipsto), expected_shape)
        END IF

        IF(.NOT. compare(SHAPE(actual_dipsto_v), expected_shape)) THEN
            CALL format_context(context, "shape("//prefix//"%dipsto_v)", SHAPE(actual_dipsto_v), expected_shape)
        END IF

    END SUBROUTINE check_dipole_block

    SUBROUTINE test_D_file( error, &
                            path, &
                            expected, &
                            coupling_id, &
                            ntarg, &
                            dipole_velocity_output, & 
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            ML_max, &
                            lplusp, &
                            xy_plane_desired, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_lrgl, &
                            LML_block_nspn, &
                            LML_block_npty, &
                            LML_block_ml, &
                            dipole_coupled, &
                            block_ind, &
                            mnp1, &
                            fintr, &
                            loc_of_blocks)

        TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
        TYPE(expected_D_file), INTENT(INOUT) :: expected
        CHARACTER(len=*), INTENT(in) :: path
        CHARACTER(len=:), ALLOCATABLE :: relative_path_header, D_file_path_header, relative_path_data, D_file_path_data                    

        INTEGER, INTENT(INOUT)                  :: ntarg
        INTEGER, INTENT(INOUT)                  :: nstmx
        INTEGER, INTENT(INOUT)                  :: no_of_L_blocks
        INTEGER, INTENT(INOUT)                  :: no_of_LML_blocks
        INTEGER, ALLOCATABLE, INTENT(INOUT)     :: mnp1(:)
        INTEGER, INTENT(INOUT)                  :: L_block_lrgl(:)
        INTEGER, INTENT(INOUT)                  :: L_block_nspn(:)
        INTEGER, INTENT(INOUT)                  :: L_block_npty(:)
        INTEGER, INTENT(INOUT)                  :: LML_block_lrgl(:)
        INTEGER, INTENT(INOUT)                  :: LML_block_nspn(:)
        INTEGER, INTENT(INOUT)                  :: LML_block_npty(:)
        INTEGER, INTENT(INOUT)                  :: LML_block_ml(:)
        INTEGER, ALLOCATABLE, INTENT(INOUT)     :: dipole_coupled(:, :)
        INTEGER, ALLOCATABLE, INTENT(INOUT)     :: block_ind(:, :, :)
        INTEGER, INTENT(IN)                     :: coupling_id
        LOGICAL, INTENT(IN)                     :: xy_plane_desired(:)
        INTEGER, INTENT(IN)                     :: ML_max
        INTEGER, INTENT(IN)                     :: lplusp
        LOGICAL, INTENT(IN)                     :: dipole_velocity_output
        INTEGER, INTENT(INOUT)                  :: fintr
        REAL(REAL64), ALLOCATABLE               :: crlv(:, :)
        REAL(REAL64), ALLOCATABLE               :: crlv_v(:, :)
        INTEGER, INTENT(IN)                     :: loc_of_blocks(:)

        REAL(wp), ALLOCATABLE :: dipsto(:, :, :) ! length gauge reduced dipole transition matrix
        REAL(wp), ALLOCATABLE :: dipsto_v(:, :, :) ! velocity gauge reduced dipole transition matrix
        INTEGER, ALLOCATABLE :: iidip(:)
        INTEGER, ALLOCATABLE :: ifdip(:)

        relative_path_header = path//"/inputs/d00"
        relative_path_data = path//"/inputs/d"
        D_file_path_header = data_file(relative_path_header)
        D_file_path_data = data_file(relative_path_data)
        
        CALL read_D_file2(D_file_path_header, &
                            D_file_path_data, &
                            coupling_id, &
                            ntarg, &
                            dipole_velocity_output, &
                            nstmx, &
                            no_of_L_blocks, &
                            no_of_LML_blocks, &
                            ML_max, &
                            lplusp, &
                            debug, &
                            xy_plane_desired, &
                            L_block_lrgl, &
                            L_block_nspn, &
                            L_block_npty, &
                            LML_block_lrgl, &
                            LML_block_nspn, &
                            LML_block_npty, &
                            LML_block_ml, &
                            crlv, &
                            crlv_v, &
                            iidip, &
                            ifdip, &
                            dipsto, &
                            dipsto_v, &
                            dipole_coupled, &
                            block_ind, &
                            mnp1, &
                            fintr)

        CALL check(error, &
                    ntarg, &
                    fintr, &
                    crlv, &
                    crlv_v, &
                    dipsto, &
                    dipsto_v, &
                    expected, &
                    nstmx, &
                    loc_of_blocks)
                    
    END SUBROUTINE test_D_file

SUBROUTINE test_atomic_small_Ne_4cores(error)
    USE initial_conditions, ONLY: xy_plane_desired
    TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
    TYPE(expected_D_file)               :: expected

    INTEGER(INT32)                      :: ntarg
    INTEGER(INT32)                      :: fintr
    INTEGER(INT32)                      :: nstmx
    INTEGER(INT32)                      :: no_of_L_blocks
    INTEGER(INT32)                      :: no_of_LML_blocks
    INTEGER, ALLOCATABLE                :: L_block_lrgl(:)
    INTEGER, ALLOCATABLE                :: L_block_nspn(:)
    INTEGER, ALLOCATABLE                :: L_block_npty(:)
    INTEGER, ALLOCATABLE                :: LML_block_lrgl(:)
    INTEGER, ALLOCATABLE                :: LML_block_nspn(:)
    INTEGER, ALLOCATABLE                :: LML_block_npty(:)
    INTEGER, ALLOCATABLE                :: LML_block_ml(:)
    INTEGER, ALLOCATABLE                :: mnp1(:)
    INTEGER, ALLOCATABLE                :: dipole_coupled(:, :)
    INTEGER, ALLOCATABLE                :: block_ind(:, :, :)
    INTEGER(INT32)                      :: coupling_id
    INTEGER(INT32)                      :: ML_max
    INTEGER(INT32)                      :: lplusp
    LOGICAL                             :: dipole_velocity_output

    INTEGER, ALLOCATABLE                :: loc_of_blocks(:)
      
    ntarg = 2
    fintr = 2
    nstmx = 254
    no_of_L_blocks = 3
    no_of_LML_blocks = 3
    
    loc_of_blocks = [1]
    L_block_lrgl = [0,1,2]
    L_block_nspn = [1,1,1]
    L_block_npty = [0,1,0]
    LML_block_lrgl = [0,1,2]
    LML_block_nspn = [1,1,1]
    LML_block_npty = [0,1,0]
    LML_block_ml = [0,0,0]
    mnp1 = [134,233,254]
    coupling_id = 1
    xy_plane_desired = .false.
    ML_max = 0
    lplusp = 0
    dipole_velocity_output = .true.

    expected = expected_D_file( &
                                ntarg=2_INT32, &
                                fintr=2_INT32,&
                                crlv=RESHAPE( &
                                    [ &
                                        0.000000E+00_REAL64, &
                                        -6.988487E-01_REAL64, &
                                        -6.988487E-01_REAL64, &
                                        0.000000E+00_REAL64 &
                                    ], &
                                    [2, 2] &
                                ), &

                                crlv_v=RESHAPE( &
                                    [ &
                                        0.000000E+00_REAL64, &
                                        7.088344E-01_REAL64, &
                                        -7.088344E-01_REAL64, &
                                        0.000000E+00_REAL64 &
                                    ], &
                                    [2, 2] &
                                ), &
    
                                blocks = [ &
                                    expected_dipole_block( &
                                        noterm=37_INT32,&
                                        isi=134_INT32,&
                                        inch=2_INT32,&
                                        jsi=233_INT32,&
                                        jnch=3_INT32,&
                                        nev=254_INT32,&
                                        nod=233_INT32,&
                                        lgsf=1_INT32,&
                                        lglf=1_INT32,&
                                        lgpf=1_INT32,&
                                        lgsi=1_INT32,&
                                        lgli=0_INT32,&
                                        lgpi=0_INT32,&
                                        dipsto=subarray( &
                                            [ &
                                                -4.407544E-04_REAL64, &
                                                -5.722607E-04_REAL64, &
                                                -1.005311E-03_REAL64, &
                                                1.974629E-05_REAL64, &
                                                2.363637E-04_REAL64, &
                                                -3.879987E-04_REAL64, &
                                                3.453868E-04_REAL64, &
                                                -1.178790E-02_REAL64, &
                                                7.320437E-04_REAL64, &
                                                6.358131E-04_REAL64, &
                                                -3.601602E-05_REAL64, &
                                                8.490597E-03_REAL64, &
                                                -9.476828E-04_REAL64, &
                                                5.239699E-03_REAL64, &
                                                8.707112E-04_REAL64, &
                                                -1.964525E-04_REAL64, &
                                                1.104426E-05_REAL64, &
                                                -2.876184E-03_REAL64, &
                                                -6.784334E-04_REAL64, &
                                                -1.383050E-03_REAL64, &
                                                -5.024296E-04_REAL64, &
                                                4.673118E-04_REAL64, &
                                                -3.007865E-04_REAL64, &
                                                5.821404E-06_REAL64, &
                                                -7.463170E-07_REAL64, &
                                                -9.812965E-05_REAL64, &
                                                1.002430E-04_REAL64 &                                            
                                            ], &
                                            [range(100,126), range(1,1), range(1,1)], &
                                            [254, 254, 2] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                -7.830317E-03_REAL64, &
                                                -5.830879E-03_REAL64, &
                                                3.372416E-03_REAL64, &
                                                -6.425970E-04_REAL64, &
                                                8.419361E-04_REAL64, &
                                                -4.231757E-04_REAL64, &
                                                -1.011228E-03_REAL64, &
                                                1.081032E-01_REAL64, &
                                                7.507378E-03_REAL64, &
                                                -7.366815E-03_REAL64, &
                                                -5.504221E-04_REAL64, &
                                                -1.162986E-01_REAL64, &
                                                -6.150746E-03_REAL64, &
                                                -1.188762E-01_REAL64, &
                                                5.652413E-03_REAL64, &
                                                7.407813E-03_REAL64, &
                                                2.285897E-04_REAL64, &
                                                1.156154E-01_REAL64, &
                                                -5.549903E-03_REAL64, &
                                                1.061402E-01_REAL64, &
                                                -4.434631E-03_REAL64, &
                                                -8.733578E-02_REAL64, &
                                                -2.394436E-03_REAL64, &
                                                -2.417936E-03_REAL64, &
                                                -5.209293E-06_REAL64, &
                                                6.008615E-02_REAL64, &
                                                8.157223E-04_REAL64 &
                                            ], &
                                            [range(100,126), range(1,1), range(1,1)], &
                                            [254, 254, 2] &  !array shape
                                        ) &
                                    ) &
                                ] &
                            )

                        CALL test_D_file(error, &
                                        "atomic_tests/small_tests/Ne_4cores", &
                                        expected, &
                                        coupling_id, &
                                        ntarg, &
                                        dipole_velocity_output, & 
                                        nstmx, &
                                        no_of_L_blocks, &
                                        no_of_LML_blocks, &
                                        ML_max, &
                                        lplusp, &
                                        xy_plane_desired, &
                                        L_block_lrgl, &
                                        L_block_nspn, &
                                        L_block_npty, &
                                        LML_block_lrgl, &
                                        LML_block_nspn, &
                                        LML_block_npty, &
                                        LML_block_ml, &
                                        dipole_coupled, &
                                        block_ind, &
                                        mnp1, &
                                        fintr, &
                                        loc_of_blocks)

        
END SUBROUTINE test_atomic_small_Ne_4cores

SUBROUTINE test_atomic_small_arx(error)
    USE initial_conditions, ONLY: xy_plane_desired
    TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
    TYPE(expected_D_file)               :: expected

    INTEGER(INT32)                      :: ntarg
    INTEGER(INT32)                      :: fintr
    INTEGER(INT32)                      :: nstmx
    INTEGER(INT32)                      :: no_of_L_blocks
    INTEGER(INT32)                      :: no_of_LML_blocks
    INTEGER, ALLOCATABLE                :: L_block_lrgl(:)
    INTEGER, ALLOCATABLE                :: L_block_nspn(:)
    INTEGER, ALLOCATABLE                :: L_block_npty(:)
    INTEGER, ALLOCATABLE                :: LML_block_lrgl(:)
    INTEGER, ALLOCATABLE                :: LML_block_nspn(:)
    INTEGER, ALLOCATABLE                :: LML_block_npty(:)
    INTEGER, ALLOCATABLE                :: LML_block_ml(:)
    INTEGER, ALLOCATABLE                :: mnp1(:)
    INTEGER, ALLOCATABLE                :: dipole_coupled(:, :)
    INTEGER, ALLOCATABLE                :: block_ind(:, :, :)
    INTEGER(INT32)                      :: coupling_id
    INTEGER(INT32)                      :: ML_max
    INTEGER(INT32)                      :: lplusp
    LOGICAL                             :: dipole_velocity_output

    INTEGER, ALLOCATABLE                :: loc_of_blocks(:)
    
    ntarg = 1
    fintr = 8
    nstmx = 141
    no_of_L_blocks = 7
    no_of_LML_blocks = 31
    
    loc_of_blocks = [4, 7]
    L_block_lrgl = [0,1,1,2,2,3,3]
    L_block_nspn = [2,2,2,2,2,2,2]
    L_block_npty = [0,0,1,0,1,0,1]
    LML_block_lrgl = [ 0,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
    LML_block_nspn = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
    LML_block_npty = [0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,1,1,1,1,1,1,1]
    LML_block_ml = [0,-1,0,1,-1,0,1,-2,-1,0,1,2,-2,-1,0,1,2,-3,-2,-1,0,1,2,3,-3,-2,-1,0,1,2,3]
    mnp1 = [49,48,95,141,94,94,139]
    coupling_id = 1
    xy_plane_desired = .false.
    ML_max = 3
    lplusp = 0
    dipole_velocity_output = .true.

    expected = expected_D_file( &
                                ntarg=1_INT32, &
                                fintr=8_INT32,&
                                crlv=RESHAPE( &
                                [ &
                                    0.000000E+00_REAL64 &
                                ], &
                                [1, 1] &
                                ) , &

                                crlv_v=RESHAPE( &
                                [ &
                                    0.000000E+00_REAL64 &
                                ], &
                                [1, 1] &
                                ), &
    
                                blocks = [ &
                                    expected_dipole_block( &
                                        noterm=47_INT32,&
                                        isi=141_INT32,&
                                        inch=3_INT32,&
                                        jsi=95_INT32,&
                                        jnch=2_INT32,&
                                        nev=141_INT32,&
                                        nod=95_INT32,&
                                        lgsf=2_INT32,&
                                        lglf=1_INT32,&
                                        lgpf=1_INT32,&
                                        lgsi=2_INT32,&
                                        lgli=2_INT32,&
                                        lgpi=0_INT32,&
                                        dipsto=subarray( &
                                            [ &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(100,126), range(1,1), range(1,1)], &
                                            [141, 141, 8] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64, &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(100,126), range(1,1), range(1,1)], &
                                            [141, 141, 8] &  !array shape
                                        ) &
                                    ), &
                                    expected_dipole_block( &
                                        noterm=47_INT32,&
                                        isi=141_INT32,&
                                        inch=3_INT32,&
                                        jsi=95_INT32,&
                                        jnch=2_INT32,&
                                        nev=141_INT32,&
                                        nod=95_INT32,&
                                        lgsf=2_INT32,&
                                        lglf=1_INT32,&
                                        lgpf=1_INT32,&
                                        lgsi=2_INT32,&
                                        lgli=2_INT32,&
                                        lgpi=0_INT32,&
                                        dipsto=subarray( &
                                            [ &
                                                -2.741742E-02_REAL64, &
                                                2.793829E-02_REAL64, &
                                                -2.552170E-02_REAL64, &
                                                1.319060E-02_REAL64, &
                                                -2.377310E-02_REAL64, &
                                                2.102869E-02_REAL64, &
                                                -6.905801E-03_REAL64, &
                                                1.621990E-02_REAL64, &
                                                -1.413167E-02_REAL64, &
                                                1.088507E-02_REAL64, &
                                                -4.119558E-04_REAL64, &
                                                7.924250E-03_REAL64, &
                                                -5.377634E-03_REAL64, &
                                                3.305218E-03_REAL64, &
                                                -3.404457E-05_REAL64, &
                                                1.786351E-03_REAL64, &
                                                -8.244402E-04_REAL64, &
                                                3.126291E-04_REAL64, &
                                                -9.092198E-05_REAL64, &
                                                -2.859419E-08_REAL64, &
                                                1.819577E-05_REAL64, &
                                                2.250782E-06_REAL64, &
                                                1.502558E-07_REAL64, &
                                                -1.124183E-08_REAL64, &
                                                3.141484E-09_REAL64, &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(25,50), range(1,1), range(1,1)], &
                                            [141, 141, 8] &
                                        ), &
                                        dipsto_v=subarray( &
                                            [ &
                                                2.985500E-01_REAL64, &
                                                -3.346857E-01_REAL64, &
                                                3.451123E-01_REAL64, &
                                                -1.905413E-01_REAL64, &
                                                3.813309E-01_REAL64, &
                                                -4.002927E-01_REAL64, &
                                                1.568275E-01_REAL64, &
                                                -3.761762E-01_REAL64, &
                                                4.056191E-01_REAL64, &
                                                -3.972727E-01_REAL64, &
                                                1.885806E-02_REAL64, &
                                                -3.789765E-01_REAL64, &
                                                3.503330E-01_REAL64, &
                                                -3.086887E-01_REAL64, &
                                                4.677525E-03_REAL64, &
                                                -2.547746E-01_REAL64, &
                                                1.930446E-01_REAL64, &
                                                -1.308509E-01_REAL64, &
                                                7.617109E-02_REAL64, &
                                                3.910642E-05_REAL64, &
                                                -3.576917E-02_REAL64, &
                                                -1.259024E-02_REAL64, &
                                                -3.151153E-03_REAL64, &
                                                5.668629E-04_REAL64, &
                                                5.007283E-05_REAL64, &
                                                0.000000E+00_REAL64 &
                                            ], &
                                            [range(25,50), range(1,1), range(1,1)], &
                                            [141, 141, 8] &  !array shape
                                        ) &
                                    ) &
                                ] &
                            )

                        CALL test_D_file(error, &
                                        "atomic_tests/small_tests/ar+", &
                                        expected, &
                                        coupling_id, &
                                        ntarg, &
                                        dipole_velocity_output, & 
                                        nstmx, &
                                        no_of_L_blocks, &
                                        no_of_LML_blocks, &
                                        ML_max, &
                                        lplusp, &
                                        xy_plane_desired, &
                                        L_block_lrgl, &
                                        L_block_nspn, &
                                        L_block_npty, &
                                        LML_block_lrgl, &
                                        LML_block_nspn, &
                                        LML_block_npty, &
                                        LML_block_ml, &
                                        dipole_coupled, &
                                        block_ind, &
                                        mnp1, &
                                        fintr, &
                                        loc_of_blocks)
        
END SUBROUTINE test_atomic_small_arx

SUBROUTINE test_atomic_small_argon(error)
    USE initial_conditions, ONLY: xy_plane_desired
    TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
    TYPE(expected_D_file)               :: expected

    INTEGER(INT32)                      :: ntarg
    INTEGER(INT32)                      :: fintr
    INTEGER(INT32)                      :: nstmx
    INTEGER(INT32)                      :: no_of_L_blocks
    INTEGER(INT32)                      :: no_of_LML_blocks
    INTEGER, ALLOCATABLE                :: L_block_lrgl(:)
    INTEGER, ALLOCATABLE                :: L_block_nspn(:)
    INTEGER, ALLOCATABLE                :: L_block_npty(:)
    INTEGER, ALLOCATABLE                :: LML_block_lrgl(:)
    INTEGER, ALLOCATABLE                :: LML_block_nspn(:)
    INTEGER, ALLOCATABLE                :: LML_block_npty(:)
    INTEGER, ALLOCATABLE                :: LML_block_ml(:)
    INTEGER, ALLOCATABLE                :: mnp1(:)
    INTEGER, ALLOCATABLE                :: dipole_coupled(:, :)
    INTEGER, ALLOCATABLE                :: block_ind(:, :, :)
    INTEGER(INT32)                      :: coupling_id
    INTEGER(INT32)                      :: ML_max
    INTEGER(INT32)                      :: lplusp
    LOGICAL                             :: dipole_velocity_output

    INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

    ntarg = 2
    fintr = 5
    nstmx = 253
    no_of_L_blocks = 5
    no_of_LML_blocks = 17

    loc_of_blocks =[1, 5]
    L_block_lrgl = [0,1,1,2,2]
    L_block_nspn = [1,1,1,1,1]
    L_block_npty = [0,0,1,0,1]
    LML_block_lrgl = [0,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2]
    LML_block_nspn = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    LML_block_npty = [0,0,0,0,1,1,1,0,0,0,0,0,1,1,1,1,1]
    LML_block_ml = [0,-1,0,1,-1,0,1,-2,-1,0,1,2,-2,-1,0,1,2]
    mnp1 = [148,110,236,253,127]
    coupling_id = 1
    xy_plane_desired = .false.
    ML_max = 2
    lplusp = 0
    dipole_velocity_output = .true.

    expected = expected_D_file( &
                                    ntarg=2_INT32, &
                                    fintr=5_INT32,&
                                    crlv=RESHAPE( &
                                    [ &
                                        0.000000E+00_REAL64, &
                                        -5.178020E-01_REAL64, &
                                        -5.178020E-01_REAL64, &
                                        0.000000E+00_REAL64 &
                                    ], &
                                    [2, 2] &
                                    ) , &

                                    crlv_v=RESHAPE( &
                                    [ &
                                        0.000000E+00_REAL64, &
                                        2.389534E-01_REAL64, &
                                        -2.389534E-01_REAL64, &
                                        0.000000E+00_REAL64 &
                                    ], &
                                    [2, 2] &
                                    ), &
    
                                blocks = [ &
                                    expected_dipole_block( &
                                        noterm=56_INT32,&
                                        isi=148_INT32,&
                                        inch=2_INT32,&
                                        jsi=236_INT32,&
                                        jnch=3_INT32,&
                                        nev=148_INT32,&!253_INT32,&
                                        nod=236_INT32,&!127_INT32,&
                                        lgsf=1_INT32,&
                                        lglf=1_INT32,&
                                        lgpf=1_INT32,&
                                        lgsi=1_INT32,&
                                        lgli=0_INT32,&
                                        lgpi=0_INT32,&
                                        dipsto=subarray( &
                                                [ &
                                                    -1.672555E-02_REAL64, &
                                                    -4.764876E-02_REAL64, &
                                                    3.258316E-03_REAL64, &
                                                    1.066131E-02_REAL64, &
                                                    5.037705E-03_REAL64, &
                                                    -8.377377E-03_REAL64, &
                                                    1.248614E-03_REAL64, &
                                                    3.894680E-03_REAL64, &
                                                    5.689523E-03_REAL64, &
                                                    4.309332E-03_REAL64, &
                                                    1.683053E-03_REAL64, &
                                                    1.802966E-03_REAL64, &
                                                    -9.228636E-03_REAL64, &
                                                    -4.318981E-03_REAL64, &
                                                    -5.386298E-03_REAL64, &
                                                    2.496381E-03_REAL64, &
                                                    -1.136382E-02_REAL64, &
                                                    -2.327930E-02_REAL64, &
                                                    9.097642E-02_REAL64, &
                                                    1.964793E-02_REAL64, &
                                                    -3.151706E-03_REAL64, &
                                                    1.403172E-03_REAL64, &
                                                    5.947901E-03_REAL64, &
                                                    9.563823E-04_REAL64, &
                                                    -7.299708E-03_REAL64, &
                                                    3.647179E-05_REAL64, &
                                                    3.931939E-03_REAL64, &
                                                    4.648901E-03_REAL64, &
                                                    -1.944171E-03_REAL64, &
                                                    -5.511340E-03_REAL64, &
                                                    6.320097E-04_REAL64, &
                                                    2.551248E-01_REAL64, &
                                                    1.120975E-01_REAL64, &
                                                    1.405717E-02_REAL64, &
                                                    -5.457185E-03_REAL64, &
                                                    1.353728E-04_REAL64, &
                                                    -1.127284E-02_REAL64, &
                                                    -2.884685E-02_REAL64, &
                                                    -4.204581E-03_REAL64, &
                                                    -5.772230E-03_REAL64, &
                                                    3.846954E-01_REAL64, &
                                                    3.025317E+00_REAL64, &
                                                    2.690688E-01_REAL64, &
                                                    -6.119765E+00_REAL64, &
                                                    4.901159E-01_REAL64, &
                                                    -1.835639E+00_REAL64, &
                                                    -1.197249E-03_REAL64, &
                                                    6.054263E-04_REAL64, &
                                                    5.232564E-03_REAL64, &
                                                    -2.510772E-01_REAL64, &
                                                    -9.038537E-04_REAL64, &
                                                    -6.387986E-03_REAL64, &
                                                    2.795532E-03_REAL64, &
                                                    -9.869379E-02_REAL64, &
                                                    -7.223816E-04_REAL64, &
                                                    -5.914256E-03_REAL64, &
                                                    1.550330E-03_REAL64, &
                                                    -5.379196E-02_REAL64, &
                                                    -5.801982E-04_REAL64, &
                                                    -5.763108E-03_REAL64, &
                                                    8.922247E-04_REAL64, &
                                                    -3.561755E-02_REAL64, &
                                                    -5.350074E-04_REAL64, &
                                                    -1.929102E-03_REAL64, &
                                                    6.476139E-04_REAL64, &
                                                    -2.323652E-02_REAL64, &
                                                    -3.600688E-04_REAL64, &
                                                    -1.004227E-02_REAL64, &
                                                    6.922332E-05_REAL64, &
                                                    -2.104322E-02_REAL64, &
                                                    -1.434375E-03_REAL64, &
                                                    -5.593544E-04_REAL64, &
                                                    1.606862E-04_REAL64, &
                                                    -1.213824E-02_REAL64, &
                                                    -2.942784E-04_REAL64, &
                                                    -9.335227E-03_REAL64, &
                                                    2.294137E-04_REAL64, &
                                                    -9.053110E-03_REAL64, &
                                                    3.365021E-04_REAL64, &
                                                    8.412696E-03_REAL64, &
                                                    2.266275E-04_REAL64, &
                                                    7.221398E-03_REAL64, &
                                                    -2.179930E-04_REAL64, &
                                                    5.898579E-03_REAL64, &
                                                    -1.641488E-04_REAL64, &
                                                    -4.907018E-03_REAL64, &
                                                    1.180083E-04_REAL64, &
                                                    -4.740049E-03_REAL64, &
                                                    1.252870E-04_REAL64, &
                                                    3.986830E-03_REAL64, &
                                                    -7.034106E-05_REAL64, &
                                                    3.046909E-03_REAL64, &
                                                    3.066329E-05_REAL64, &
                                                    2.288508E-03_REAL64, &
                                                    7.134695E-06_REAL64, &
                                                    -1.622276E-03_REAL64, &
                                                    3.948067E-06_REAL64, &
                                                    -2.163164E-03_REAL64, &
                                                    -5.784570E-05_REAL64, &
                                                    1.072402E-03_REAL64, &
                                                    -6.491075E-06_REAL64 &
                                                ], &
                                                [range(28,128), range(119,119), range(1,1)], &
                                                [253, 253, 5] &
                                        ), &

                                        dipsto_v=subarray( &
                                            [ &
                                                -8.113238E-02_REAL64, &
                                                -2.356247E-02_REAL64, &
                                                -9.722851E-02_REAL64, &
                                                -8.152114E-02_REAL64, &
                                                7.564886E-02_REAL64, &
                                                9.068106E-02_REAL64, &
                                                2.608631E-02_REAL64, &
                                                1.164348E-01_REAL64, &
                                                -5.458490E-02_REAL64, &
                                                8.456954E-01_REAL64, &
                                                -2.035323E-02_REAL64, &
                                                1.854544E-01_REAL64, &
                                                -1.536515E-01_REAL64, &
                                                7.784576E-02_REAL64, &
                                                4.220535E-02_REAL64, &
                                                1.023078E-03_REAL64, &
                                                6.787052E-02_REAL64, &
                                                1.392114E-04_REAL64, &
                                                2.770086E-03_REAL64 &
                                            ], &
                                            [range(10,28), range(50,50), range(1,1)], &
                                            [253,253,5] &  !array shape
                                        ) &
                                    ), &

                                    expected_dipole_block( &
                                        noterm=56_INT32,&
                                        isi=148_INT32,&
                                        inch=2_INT32,&
                                        jsi=236_INT32,&
                                        jnch=3_INT32,&
                                        nev=148_INT32,&!253_INT32,&
                                        nod=236_INT32,&!127_INT32,&
                                        lgsf=1_INT32,&
                                        lglf=1_INT32,&
                                        lgpf=1_INT32,&
                                        lgsi=1_INT32,&
                                        lgli=0_INT32,&
                                        lgpi=0_INT32,&
                                        dipsto=subarray( &
                                                [ &
                                                    -1.672555E-02_REAL64, &
                                                    -4.764876E-02_REAL64, &
                                                    3.258316E-03_REAL64, &
                                                    1.066131E-02_REAL64, &
                                                    5.037705E-03_REAL64, &
                                                    -8.377377E-03_REAL64, &
                                                    1.248614E-03_REAL64, &
                                                    3.894680E-03_REAL64, &
                                                    5.689523E-03_REAL64, &
                                                    4.309332E-03_REAL64, &
                                                    1.683053E-03_REAL64, &
                                                    1.802966E-03_REAL64, &
                                                    -9.228636E-03_REAL64, &
                                                    -4.318981E-03_REAL64, &
                                                    -5.386298E-03_REAL64, &
                                                    2.496381E-03_REAL64, &
                                                    -1.136382E-02_REAL64, &
                                                    -2.327930E-02_REAL64, &
                                                    9.097642E-02_REAL64, &
                                                    1.964793E-02_REAL64, &
                                                    -3.151706E-03_REAL64, &
                                                    1.403172E-03_REAL64, &
                                                    5.947901E-03_REAL64, &
                                                    9.563823E-04_REAL64, &
                                                    -7.299708E-03_REAL64, &
                                                    3.647179E-05_REAL64, &
                                                    3.931939E-03_REAL64, &
                                                    4.648901E-03_REAL64, &
                                                    -1.944171E-03_REAL64, &
                                                    -5.511340E-03_REAL64, &
                                                    6.320097E-04_REAL64, &
                                                    2.551248E-01_REAL64, &
                                                    1.120975E-01_REAL64, &
                                                    1.405717E-02_REAL64, &
                                                    -5.457185E-03_REAL64, &
                                                    1.353728E-04_REAL64, &
                                                    -1.127284E-02_REAL64, &
                                                    -2.884685E-02_REAL64, &
                                                    -4.204581E-03_REAL64, &
                                                    -5.772230E-03_REAL64, &
                                                    3.846954E-01_REAL64, &
                                                    3.025317E+00_REAL64, &
                                                    2.690688E-01_REAL64, &
                                                    -6.119765E+00_REAL64, &
                                                    4.901159E-01_REAL64, &
                                                    -1.835639E+00_REAL64, &
                                                    -1.197249E-03_REAL64, &
                                                    6.054263E-04_REAL64, &
                                                    5.232564E-03_REAL64, &
                                                    -2.510772E-01_REAL64, &
                                                    -9.038537E-04_REAL64, &
                                                    -6.387986E-03_REAL64, &
                                                    2.795532E-03_REAL64, &
                                                    -9.869379E-02_REAL64, &
                                                    -7.223816E-04_REAL64, &
                                                    -5.914256E-03_REAL64, &
                                                    1.550330E-03_REAL64, &
                                                    -5.379196E-02_REAL64, &
                                                    -5.801982E-04_REAL64, &
                                                    -5.763108E-03_REAL64, &
                                                    8.922247E-04_REAL64, &
                                                    -3.561755E-02_REAL64, &
                                                    -5.350074E-04_REAL64, &
                                                    -1.929102E-03_REAL64, &
                                                    6.476139E-04_REAL64, &
                                                    -2.323652E-02_REAL64, &
                                                    -3.600688E-04_REAL64, &
                                                    -1.004227E-02_REAL64, &
                                                    6.922332E-05_REAL64, &
                                                    -2.104322E-02_REAL64, &
                                                    -1.434375E-03_REAL64, &
                                                    -5.593544E-04_REAL64, &
                                                    1.606862E-04_REAL64, &
                                                    -1.213824E-02_REAL64, &
                                                    -2.942784E-04_REAL64, &
                                                    -9.335227E-03_REAL64, &
                                                    2.294137E-04_REAL64, &
                                                    -9.053110E-03_REAL64, &
                                                    3.365021E-04_REAL64, &
                                                    8.412696E-03_REAL64, &
                                                    2.266275E-04_REAL64, &
                                                    7.221398E-03_REAL64, &
                                                    -2.179930E-04_REAL64, &
                                                    5.898579E-03_REAL64, &
                                                    -1.641488E-04_REAL64, &
                                                    -4.907018E-03_REAL64, &
                                                    1.180083E-04_REAL64, &
                                                    -4.740049E-03_REAL64, &
                                                    1.252870E-04_REAL64, &
                                                    3.986830E-03_REAL64, &
                                                    -7.034106E-05_REAL64, &
                                                    3.046909E-03_REAL64, &
                                                    3.066329E-05_REAL64, &
                                                    2.288508E-03_REAL64, &
                                                    7.134695E-06_REAL64, &
                                                    -1.622276E-03_REAL64, &
                                                    3.948067E-06_REAL64, &
                                                    -2.163164E-03_REAL64, &
                                                    -5.784570E-05_REAL64, &
                                                    1.072402E-03_REAL64, &
                                                    -6.491075E-06_REAL64 &
                                                ], &
                                                [range(28,128), range(119,119), range(1,1)], &
                                                [253, 253, 5] &
                                        ), &

                                        dipsto_v=subarray( &
                                            [ &
                                                -8.113238E-02_REAL64, &
                                                -2.356247E-02_REAL64, &
                                                -9.722851E-02_REAL64, &
                                                -8.152114E-02_REAL64, &
                                                7.564886E-02_REAL64, &
                                                9.068106E-02_REAL64, &
                                                2.608631E-02_REAL64, &
                                                1.164348E-01_REAL64, &
                                                -5.458490E-02_REAL64, &
                                                8.456954E-01_REAL64, &
                                                -2.035323E-02_REAL64, &
                                                1.854544E-01_REAL64, &
                                                -1.536515E-01_REAL64, &
                                                7.784576E-02_REAL64, &
                                                4.220535E-02_REAL64, &
                                                1.023078E-03_REAL64, &
                                                6.787052E-02_REAL64, &
                                                1.392114E-04_REAL64, &
                                                2.770086E-03_REAL64 &
                                            ], &
                                            [range(10,28), range(50,50), range(1,1)], &
                                            [253,253,5] &  !array shape
                                        ) &
                                    ) &
                                ] &
                            )

                    CALL test_D_file(error, &
                                    "atomic_tests/small_tests/argon", &
                                    expected, &
                                    coupling_id, &
                                    ntarg, &
                                    dipole_velocity_output, & 
                                    nstmx, &
                                    no_of_L_blocks, &
                                    no_of_LML_blocks, &
                                    ML_max, &
                                    lplusp, &
                                    xy_plane_desired, &
                                    L_block_lrgl, &
                                    L_block_nspn, &
                                    L_block_npty, &
                                    LML_block_lrgl, &
                                    LML_block_nspn, &
                                    LML_block_npty, &
                                    LML_block_ml, &
                                    dipole_coupled, &
                                    block_ind, &
                                    mnp1, &
                                    fintr, &
                                    loc_of_blocks)
    
END SUBROUTINE test_atomic_small_argon

SUBROUTINE test_atomic_small_helium(error)

    USE initial_conditions, ONLY: xy_plane_desired
    TYPE(error_type), ALLOCATABLE, INTENT(out) :: error
    TYPE(expected_D_file)               :: expected

    INTEGER(INT32)                      :: ntarg
    INTEGER(INT32)                      :: fintr
    INTEGER(INT32)                      :: nstmx
    INTEGER(INT32)                      :: no_of_L_blocks
    INTEGER(INT32)                      :: no_of_LML_blocks
    INTEGER, ALLOCATABLE                :: L_block_lrgl(:)
    INTEGER, ALLOCATABLE                :: L_block_nspn(:)
    INTEGER, ALLOCATABLE                :: L_block_npty(:)
    INTEGER, ALLOCATABLE                :: LML_block_lrgl(:)
    INTEGER, ALLOCATABLE                :: LML_block_nspn(:)
    INTEGER, ALLOCATABLE                :: LML_block_npty(:)
    INTEGER, ALLOCATABLE                :: LML_block_ml(:)
    INTEGER, ALLOCATABLE                :: mnp1(:)
    INTEGER, ALLOCATABLE                :: dipole_coupled(:, :)
    INTEGER, ALLOCATABLE                :: block_ind(:, :, :)
    INTEGER(INT32)                      :: coupling_id
    INTEGER(INT32)                      :: ML_max
    INTEGER(INT32)                      :: lplusp
    LOGICAL                             :: dipole_velocity_output

    INTEGER, ALLOCATABLE                :: loc_of_blocks(:)

    ntarg = 3
    fintr = 3
    nstmx = 154
    no_of_L_blocks = 4
    no_of_LML_blocks = 4

    loc_of_blocks = [1]
    L_block_lrgl = [0,1,2,3]
    L_block_nspn = [1,1,1,1]
    L_block_npty = [0,1,0,1]
    LML_block_lrgl = [0,1,2,3]
    LML_block_nspn = [1,1,1,1]
    LML_block_npty = [0,1,0,1]
    LML_block_ml = [0,0,0,0]
    mnp1 = [118,154,152,148]
    coupling_id = 1
    xy_plane_desired = .false.
    ML_max = 0
    lplusp = 0
    dipole_velocity_output = .true.

    expected = expected_D_file( &
                                ntarg=3_INT32, &
                                fintr=3_INT32,&
                                crlv=RESHAPE( &
                                [ &
                                    0.000000E+00_REAL64, &
                                    8.660254E-01_REAL64, &
                                    0.000000E+00_REAL64, &
                                    8.660254E-01_REAL64, &
                                    0.000000E+00_REAL64, &
                                    -1.000000E+00_REAL64, &
                                    0.000000E+00_REAL64, &
                                    -1.000000E+00_REAL64, &
                                    0.000000E+00_REAL64 &
                                ], &
                                [3, 3] &
                                ) , &

                                crlv_v=RESHAPE( &
                                [ &
                                    0.000000E+00_REAL64, &
                                    -1.732051E+00_REAL64, &
                                    0.000000E+00_REAL64, &
                                    1.732051E+00_REAL64, &
                                    0.000000E+00_REAL64, &
                                    9.999999E-01_REAL64, &
                                    0.000000E+00_REAL64, &
                                    -9.999999E-01_REAL64, &
                                    0.000000E+00_REAL64 &
                                ], &
                                [3, 3] &
                                ), &
    
                                blocks = [ &
                                    expected_dipole_block( &
                                        noterm=38_INT32,&
                                        isi=118_INT32,&
                                        inch=3_INT32,&
                                        jsi=154_INT32,&
                                        jnch=4_INT32,&
                                        nev=118_INT32,&
                                        nod=154_INT32,&
                                        lgsf=1_INT32,&
                                        lglf=1_INT32,&
                                        lgpf=1_INT32,&
                                        lgsi=1_INT32,&
                                        lgli=0_INT32,&
                                        lgpi=0_INT32,&
                                        dipsto=subarray( &
                                                [ &
                                                    1.660482E-02_REAL64, &
                                                    -4.089755E-01_REAL64, &
                                                    -3.618911E-02_REAL64, &
                                                    2.003335E-01_REAL64, &
                                                    2.729119E-01_REAL64, &
                                                    9.342146E-01_REAL64, &
                                                    -3.686505E+00_REAL64, &
                                                    -2.988550E+00_REAL64, &
                                                    -2.377787E-01_REAL64, &
                                                    1.029872E+00_REAL64, &
                                                    -6.187766E+00_REAL64, &
                                                    -2.964204E+00_REAL64, &
                                                    3.625551E-01_REAL64, &
                                                    -3.215959E-01_REAL64, &
                                                    -2.853457E-01_REAL64, &
                                                    4.180491E-01_REAL64, &
                                                    4.077365E-01_REAL64, &
                                                    1.344632E+00_REAL64, &
                                                    -8.926908E-02_REAL64, &
                                                    1.364339E-01_REAL64, &
                                                    5.702827E-02_REAL64, &
                                                    1.461093E-02_REAL64, &
                                                    -1.126028E-01_REAL64, &
                                                    -8.282860E-02_REAL64, &
                                                    9.185173E-02_REAL64, &
                                                    2.635103E-01_REAL64, &
                                                    -7.105101E-01_REAL64, &
                                                    -1.504636E-01_REAL64, &
                                                    -5.629408E-01_REAL64, &
                                                    -6.496983E-02_REAL64, &
                                                    -1.491827E-01_REAL64, &
                                                    2.298300E-01_REAL64, &
                                                    5.179414E+00_REAL64, &
                                                    2.888719E+00_REAL64, &
                                                    -5.968648E+00_REAL64, &
                                                    -7.535335E-01_REAL64, &
                                                    1.760952E-01_REAL64, &
                                                    1.063844E-01_REAL64, &
                                                    -9.722289E-01_REAL64, &
                                                    1.350583E-01_REAL64, &
                                                    -6.293847E-02_REAL64, &
                                                    1.301363E-01_REAL64, &
                                                    2.655712E-01_REAL64, &
                                                    8.118707E-02_REAL64, &
                                                    1.423859E-01_REAL64, &
                                                    5.166609E-02_REAL64, &
                                                    -1.683673E-01_REAL64, &
                                                    1.311416E-01_REAL64, &
                                                    9.270706E-02_REAL64, &
                                                    9.910755E-02_REAL64, &
                                                    -1.640815E-01_REAL64, &
                                                    -3.852290E-01_REAL64, &
                                                    6.277760E-01_REAL64, &
                                                    -6.072983E-01_REAL64, &
                                                    -5.289467E-01_REAL64, &
                                                    5.818915E-01_REAL64, &
                                                    1.661311E+00_REAL64, &
                                                    -2.096217E+00_REAL64, &
                                                    1.355720E+00_REAL64, &
                                                    -4.743039E-01_REAL64, &
                                                    -1.758390E-01_REAL64, &
                                                    -8.738682E-01_REAL64, &
                                                    1.920697E-01_REAL64, &
                                                    -3.411759E-01_REAL64, &
                                                    -2.398758E-01_REAL64, &
                                                    6.880560E-01_REAL64, &
                                                    2.371902E-02_REAL64, &
                                                    1.364374E-01_REAL64, &
                                                    5.959430E-02_REAL64 &
                                                ], &
                                                [range(8,30), range(20,22), range(1,1)], &
                                                [154, 154, 3] &
                                        ), &

                                        dipsto_v=subarray( &
                                            [ &
                                                -1.267627E-02_REAL64, &
                                                1.558239E-01_REAL64, &
                                                4.599738E-02_REAL64, &
                                                8.486325E-03_REAL64, &
                                                -1.132159E-02_REAL64, &
                                                -1.617267E-01_REAL64, &
                                                -5.912931E-02_REAL64, &
                                                -2.393870E-02_REAL64, &
                                                4.818550E-01_REAL64, &
                                                -9.546424E-02_REAL64, &
                                                1.669244E-01_REAL64, &
                                                -1.910578E-01_REAL64, &
                                                -8.112591E-02_REAL64, &
                                                -1.960282E-01_REAL64, &
                                                -3.611982E-01_REAL64, &
                                                -3.939755E-01_REAL64, &
                                                -1.364136E+00_REAL64, &
                                                3.919333E-01_REAL64, &
                                                3.593551E-01_REAL64, &
                                                -3.156719E-01_REAL64, &
                                                3.074657E-01_REAL64, &
                                                1.879140E-01_REAL64, &
                                                -2.922501E-02_REAL64, &
                                                -1.085718E-01_REAL64, &
                                                4.207230E-02_REAL64, &
                                                6.181582E-02_REAL64, &
                                                1.635967E-02_REAL64, &
                                                7.472395E-02_REAL64, &
                                                -4.439890E-03_REAL64, &
                                                -4.896031E-02_REAL64, &
                                                2.709098E-01_REAL64, &
                                                -8.235570E-02_REAL64, &
                                                2.713366E-02_REAL64, &
                                                -3.487189E-02_REAL64, &
                                                2.281302E-02_REAL64, &
                                                4.490759E-02_REAL64, &
                                                -4.660654E-03_REAL64, &
                                                -3.656305E-02_REAL64, &
                                                2.174817E-03_REAL64, &
                                                2.050256E-01_REAL64, &
                                                1.052854E-01_REAL64, &
                                                -2.730590E-02_REAL64, &
                                                4.023812E-02_REAL64, &
                                                -1.008042E-02_REAL64, &
                                                -2.987866E-02_REAL64, &
                                                -2.504021E-02_REAL64, &
                                                -2.668450E-02_REAL64, &
                                                1.417238E-02_REAL64, &
                                                -2.798110E-02_REAL64, &
                                                -6.145649E-02_REAL64 &
                                            ], &
                                            [range(10,10), range(20,69), range(1,1)], &
                                            [154,154, 3] &  !array shape
                                        ) &
                                    ) &
                                ] &
                            )

                    CALL test_D_file(error, &
                                    "atomic_tests/small_tests/helium", &
                                    expected, &
                                    coupling_id, &
                                    ntarg, &
                                    dipole_velocity_output, & 
                                    nstmx, &
                                    no_of_L_blocks, &
                                    no_of_LML_blocks, &
                                    ML_max, &
                                    lplusp, &
                                    xy_plane_desired, &
                                    L_block_lrgl, &
                                    L_block_nspn, &
                                    L_block_npty, &
                                    LML_block_lrgl, &
                                    LML_block_nspn, &
                                    LML_block_npty, &
                                    LML_block_ml, &
                                    dipole_coupled, &
                                    block_ind, &
                                    mnp1, &
                                    fintr, &
                                    loc_of_blocks)

END SUBROUTINE test_atomic_small_helium

END MODULE
