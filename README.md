\page quick_start

WARNING NOTE:  This branch is slowly being modifed as MP's ecse changes are added file by file. Do not expect to be able to compile and run this branch without checking this file first.  (12-04-2022)


Quick start Guide 
================

Obtain RMT
==========

To obtain a copy of the RMT repository, change into the directory which will house the repository and type:

    git clone https://gitlab.com/Uk-amor/RMT/rmt

Upon successful download you will see the line:

    Checking out files: 100% (/), done

and the new directory `rmt` will appear in your current directory.


Compile RMT
===========

Prerequisites
-------------

Before attempting to compile the RMT code, you will need to have access to the following:

1. A parallel FORTRAN compiler
2. A C compiler
3. CMake version 3.0 or higher
4. LAPACK and BLAS libraries

Compile Instructions
--------------------

Firstly, set up the compilation environment. Type:

    export CC=$(which mpicc)
    export CXX=$(which mpicxx)
    export FC=$(which mpif90)

All three compilers should be from the same suite (eg Intel).

The build of RMT is managed with cmake. To compile RMT create a new directory
`build` and call CMake:

    cd rmt
    mkdir build
    cd build
    cmake ../source
    make 

Upon successful compilation, the main executable `rmt.x` will reside in `rmt/build/bin`. 
Documentation will also be available in docs/html/index.html

Note that building the documentation requires [Doxygen](https://www.doxygen.nl)

### Unit Tests

If you want to build unit tests, or enable compilation flags useful for debugging,
you should pass `-D RMT_TESTING=ON` to CMake, i.e. replace the above with:

    cd rmt
    mkdir build
    cd build
    cmake -D RMT_TESTING=ON ../source
    make

Now there will be an extra executable in `rmt/build/bin` called `RMT-tester`. Without
arguments, i.e.

    rmt/build/bin/RMT-tester

this will run all tests. The first argument selects a suite name, e.g.

    rmt/build/bin/RMT-tester utilities

will run all tests in the `utilities` suite. The second argument selects a test name,
e.g.

    rmt/build/bin/RMT-tester utilities int-to-char::zero

will run the `int-to-char::zero` test of the `utilities` suite.

Run RMT
-------

To check that your version of RMT has compiled correctly you may wish to run a few tests.
In the `tests` directory a variety of small and large, atomic and molecular input files 
and expected outputs are available. To test your RMT build, execute, for example:

    cd tests/atomic_tests/small_tests/Ne_4cores
    mkdir test
    cd test
    ln -s ../inputs/* .
    mpiexec -n 4 ~/<path_to_repo>/rmt/build/bin/rmt.x > log.out

You can now check the agreement between the output files in the `test` directory with the
expected ouptuts in `rmt_output`.


Utility suite
-------------

A suite of python utilities is provided for running and testing RMT calculations,
and postprocessing the data they produce. The suite can be installed with

    cd <path_to_repo>
    pip install .

Documentation for the utility suite can be generated with 

    pip install -r requirements-dev.txt
    make -C <path_to_repo>/docs html

Note that this requires a working python installation and pip.

\defgroup source
